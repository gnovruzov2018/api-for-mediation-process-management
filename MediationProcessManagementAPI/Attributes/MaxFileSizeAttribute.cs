﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Attributes
{
    public class MaxFileSizeAttribute : ValidationAttribute
    {
        private readonly int _maxFileSize;
        public MaxFileSizeAttribute(int maxFileSize)
        {
            _maxFileSize = maxFileSize;
        }

        protected override ValidationResult IsValid(
        object value, ValidationContext validationContext)
        {
            var files = value as List<IFormFile>;
            if (files != null && files.Count > 0)
            {
                int maxFileSizeInBytes = _maxFileSize * 1024 * 1024;
                if (files.Sum(f => f.Length) > maxFileSizeInBytes)
                {
                    return new ValidationResult(GetErrorMessage());
                }
            }

            return ValidationResult.Success;
        }

        public string GetErrorMessage()
        {
            return $"Seçilmiş sənədlərin ümumi ölçüsü { _maxFileSize}MB-dan çox ola bilməz";
        }
    }
}
