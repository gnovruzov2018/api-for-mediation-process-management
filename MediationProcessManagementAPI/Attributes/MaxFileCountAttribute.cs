﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Attributes
{
    public class MaxFileCountAttribute : ValidationAttribute
    {
        private readonly int _maxFileCount;
        public MaxFileCountAttribute(int maxFileCount)
        {
            _maxFileCount = maxFileCount;
        }

        protected override ValidationResult IsValid(
        object value, ValidationContext validationContext)
        {
            var files = value as List<IFormFile>;
            if (files != null && files.Count > 0)
            {
                if (files.Count > _maxFileCount)
                {
                    return new ValidationResult(GetErrorMessage());
                }
            }
            return ValidationResult.Success;
        }

        public string GetErrorMessage()
        {
            return $"Seçilmiş sənədlərin sayı { _maxFileCount}-dən çox ola bilməz";
        }
    }
}
