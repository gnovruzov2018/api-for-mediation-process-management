﻿using RegistryServiceNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Interfaces
{
    public interface IRegistryService
    {
        public Task<FullPerson> GetPersonByPin(string pin);
        public Task<FullDocument> GetFullDocByNumber(string series, string number);
    }
}
