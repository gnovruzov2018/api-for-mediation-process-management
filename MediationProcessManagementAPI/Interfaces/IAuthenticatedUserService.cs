﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Interfaces
{
    public interface IAuthenticatedUserService
    {
        int UserId { get; }
        string PersonId { get; }
        string MediatrId { get; }
        string OfficeId { get; }
        string RoleName { get; }
    }
}
