﻿using MediationProcessManagementAPI.Models.Dtos.Account;
using MediationProcessManagementAPI.Wrappers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Interfaces
{
    public interface IAccountService
    {
        Task<Response<AuthenticationResponse>> AuthenticateCitizenAsync(CitizenAuthenticationRequest request, string ipAddress);
        Task<Response<AuthenticationResponse>> AuthenticateUserAsync(UserAuthenticationRequest request, string ipAddress);
        Task<Response<AuthenticationResponse>> RefreshToken(string token, string ipAddress);
        Task<Response<object>> RevokeToken(string token, string ipAddress);

    }
}
