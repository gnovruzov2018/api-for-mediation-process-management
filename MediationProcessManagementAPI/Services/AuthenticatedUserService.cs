﻿using MediationProcessManagementAPI.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace MediationProcessManagementAPI.Services
{
    public class AuthenticatedUserService : IAuthenticatedUserService
    {
        public AuthenticatedUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = Convert.ToInt32(httpContextAccessor.HttpContext?.User?.FindFirstValue("uid"));
            PersonId = httpContextAccessor.HttpContext?.User?.FindFirstValue("personId");
            MediatrId = httpContextAccessor.HttpContext?.User?.FindFirstValue("mediatrId");
            OfficeId = httpContextAccessor.HttpContext?.User?.FindFirstValue("officeId");
            RoleName = httpContextAccessor.HttpContext?.User?.FindFirstValue("role");
        }

        public int UserId { get; }
        public string PersonId { get; }

        public string MediatrId { get; }

        public string OfficeId { get; }

        public string RoleName { get; }

    }
}
