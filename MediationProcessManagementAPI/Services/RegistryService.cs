﻿using RegistryServiceNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceModel;
using MediationProcessManagementAPI.Interfaces;

namespace MediationProcessManagementAPI.Services
{
    public class RegistryService : IRegistryService
    {
        private static string username = "exidmet";
        private static string password = "Ex!dm3t2R";
        private BasicHttpBinding httpBinding = null;
        private EndpointAddress endpointAddress = null;
        private RegistryEXidmetServiceClient client;

        public RegistryService()
        {
            httpBinding = new BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential);
            httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            endpointAddress = new EndpointAddress(new Uri("https://registryservice.gov.az:10443/RegistryEXidmetService.svc"));
            client = new RegistryEXidmetServiceClient(httpBinding, endpointAddress);
        }

        public async Task<FullPerson> GetPersonByPin(string pin)
        {
            FullPerson person = new FullPerson();
            if (!string.IsNullOrEmpty(pin))
            {
                 
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;

                SearchPerson search_person = new SearchPerson() { Pin = pin };
                SearchDocument document = new SearchDocument() { Person = search_person };
                SearchParam param = new SearchParam()
                {
                    Document = document,
                    SearchType = SearchType.PersonByPin
                };

                FullPersonSearchResult result = await client.GetOnePersonAsync(param);
                person = result.Person;

                await client.CloseAsync();
            }
            return person;
        }

        public async Task<FullDocument> GetFullDocByNumber(string series, string number)
        {
            FullDocument resDocument = new FullDocument();
            if (!string.IsNullOrEmpty(series) && !string.IsNullOrEmpty(number))
            {
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;

                SearchParam param = new SearchParam();
                param.SearchType = SearchType.DocumentByNumber;

                SearchDocument document = new SearchDocument();


                document.IsAddressSpecified = true;
                document.IsPhotoSpecified = true;
                if (series == "AA")
                {
                    document.Number = series + number;
                }
                else
                {
                    document.Series = series;
                    document.Number = number;
                }

                param.Document = document;

                FullDocSearchResult result = await client.GetOneDocumentAsync(param);
                resDocument = result.Document;

                await client.CloseAsync();
            }
            return resDocument;
        }
    }
}
