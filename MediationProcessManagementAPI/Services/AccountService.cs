﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Interfaces;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos.Account;
using MediationProcessManagementAPI.Models.Enums;
using MediationProcessManagementAPI.Models.Errors;
using MediationProcessManagementAPI.Settings;
using MediationProcessManagementAPI.Wrappers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Services
{
    public class AccountService : IAccountService
    {
        private readonly IRegistryService registryService;
        private readonly IUnitOfWork unitOfWork;
        private readonly JWTSettings jwtSettings;
        private List<Expression<Func<User, object>>> includesList;

        public AccountService(IUnitOfWork unitOfWork,
            IOptions<JWTSettings> jwtSettings,
            IRegistryService registryService)
        {
            this.unitOfWork = unitOfWork;
            this.jwtSettings = jwtSettings.Value;
            this.registryService = registryService;
            includesList = new List<Expression<Func<User, object>>>() { x => x.Role};
        }

        public async Task<Response<AuthenticationResponse>> AuthenticateCitizenAsync(CitizenAuthenticationRequest request, string ipAddress)
        {
            includesList.Add(x => x.Person);
            User user = await unitOfWork.Users.GetOneAsync(predicate: x => x.Person.DocSeries == request.DocSeries && 
                x.Person.DocNumber == request.DocNumber && x.Person.PIN == request.PIN && x.Person.DateOfBirth == request.DateOfBirth,
                includes: includesList);

            if(user == null)
            {
                var personFromRegistry = await registryService.GetFullDocByNumber(request.DocSeries, request.DocNumber);
                if (personFromRegistry == null)
                    throw new ApiException($"Daxil etdiyiniz şəxsiyyət vəsiqəsi məlumatları yanlışdır");

                if(personFromRegistry.Person.Pin != request.PIN || personFromRegistry.Person.DateOfBirthStr != request.DateOfBirth)
                    throw new ApiException($"Daxil etdiyiniz şəxsiyyət vəsiqəsi məlumatları yanlışdır");

                if(personFromRegistry.Status != RegistryServiceNS.Status.Active)
                    throw new ApiException($"Şəxsiyyət vəsiqəsi qüvvədən düşmüşdür");

                User newUser = new User
                {
                    RoleId = 5,
                    Person = new Person
                    {
                        FirstName = personFromRegistry.Person.FirstName,
                        LastName = personFromRegistry.Person.LastName,
                        MiddleName = personFromRegistry.Person.MiddleName,
                        DateOfBirth = personFromRegistry.Person.DateOfBirthStr,
                        DocSeries = request.DocSeries,
                        DocNumber = request.DocNumber,
                        PIN = request.PIN,
                        Gender = (Gender)personFromRegistry.Person.Gender,
                        Image = personFromRegistry.Photo.FileBinary,
                        RegAddress = personFromRegistry.Address.FullAddress
                    }
                };

                var createdUser = await unitOfWork.Users.AddAsync(newUser);

                user = await unitOfWork.Users.GetOneAsync(predicate: x => x.Id == createdUser.Id, includes: includesList);
            }

            JwtSecurityToken jwtSecurityToken = GenerateJWToken(user);
            AuthenticationResponse response = new AuthenticationResponse();
            response.UserId = user.Id;
            response.JWToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
            response.Email = user.Person?.Email;
            response.Login = user.Login;
            response.Role = user.Role.RoleName;
            response.Person = user.Person;
            var refreshToken = GenerateRefreshToken(ipAddress, user.Id);
            await unitOfWork.RefreshTokens.AddAsync(refreshToken);
            response.RefreshToken = refreshToken.Token;
            return new Response<AuthenticationResponse>(response, $"Uğurlu giriş");
        }

        public async Task<Response<AuthenticationResponse>> AuthenticateUserAsync(UserAuthenticationRequest request, string ipAddress)
        {
            includesList.Add(x => x.Office);
            includesList.Add(x => x.Mediatr);

            var user = await unitOfWork.Users.GetOneAsync(predicate: x => x.Login == request.Login && x.Password == request.Password,
                includes: includesList);

            if (user == null)
                throw new ApiException("Daxil etdiyiniz giriş parametrləri yanlışdır");

            JwtSecurityToken jwtSecurityToken = GenerateJWToken(user);
            AuthenticationResponse response = new AuthenticationResponse();
            response.UserId = user.Id;
            response.JWToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
            response.Login = user.Login;
            response.Role = user.Role.RoleName;
            response.Mediatr = user.Mediatr;
            response.Office = user.Office;
            var refreshToken = GenerateRefreshToken(ipAddress, user.Id);
            await unitOfWork.RefreshTokens.AddAsync(refreshToken);
            response.RefreshToken = refreshToken.Token;
            return new Response<AuthenticationResponse>(response, $"Uğurlu giriş");
        }

        public async Task<Response<AuthenticationResponse>> RefreshToken(string token, string ipAddress)
        {
            var refreshTokenFromDb = await unitOfWork.RefreshTokens.GetOneAsync(predicate: x => x.Token == token);

            // return null if no user found with token
            if (refreshTokenFromDb == null || !refreshTokenFromDb.IsActive)
                throw new ApiException($"'Refresh token' ya yanlışdır ya da aktiv deyil");

            // get user by id
            includesList.Add(x => x.Office);
            includesList.Add(x => x.Mediatr);
            includesList.Add(x => x.Person);
            var user = await unitOfWork.Users.GetOneAsync(predicate: x => x.Id == refreshTokenFromDb.UserId, includes: includesList);

            // replace old refresh token with a new one and save
            var newRefreshToken = GenerateRefreshToken(ipAddress, user.Id);
            refreshTokenFromDb.Revoked = DateTime.Now;
            refreshTokenFromDb.RevokedByIp = ipAddress;
            refreshTokenFromDb.ReplacedByToken = newRefreshToken.Token;
            await unitOfWork.RefreshTokens.AddAsync(newRefreshToken);
            await unitOfWork.RefreshTokens.UpdateAsync(refreshTokenFromDb);

            // generate new jwt
            
            var jwtToken = GenerateJWToken(user);

            var response = new AuthenticationResponse();
            response.UserId = user.Id;
            response.JWToken = new JwtSecurityTokenHandler().WriteToken(jwtToken);
            response.Email = user.Person?.Email;
            response.Login = user.Login;
            response.Role = user.Role.RoleName;
            response.RefreshToken = newRefreshToken.Token;
            response.Office = user.Office;
            response.Person = user.Person;
            response.Mediatr = user.Mediatr;
            return new Response<AuthenticationResponse>(response, $"Access token uğurla yeniləndi");
        }

        public async Task<Response<object>> RevokeToken(string token, string ipAddress)
        {
            if(string.IsNullOrEmpty(token))
                throw new ApiException($"'Refresh token' boş ola bilməz");

            var refreshTokenFromDb = await unitOfWork.RefreshTokens.GetOneAsync(predicate: x => x.Token == token);

            // return null if no user found with token
            if (refreshTokenFromDb == null || !refreshTokenFromDb.IsActive)
                throw new ApiException($"'Refresh token' ya yanlışdır ya da aktiv deyil");

            // revoke token and save
            refreshTokenFromDb.Revoked = DateTime.Now;
            refreshTokenFromDb.RevokedByIp = ipAddress;
            await unitOfWork.RefreshTokens.UpdateAsync(refreshTokenFromDb);

            return new Response<object>(new { Message = "'Refresh token' uğurla ləğv edildi" }, $"'Refresh token' uğurla ləğv edildi");
        }

        private JwtSecurityToken GenerateJWToken(User user)
        {

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("uid", user.Id.ToString()),
                new Claim("personId", user.PersonId.ToString()),
                new Claim("mediatrId", user.MediatrId.ToString()),
                new Claim("officeId", user.OfficeId.ToString()),
                new Claim("role", user.Role.RoleName)
            };

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha512Signature);

            var jwtSecurityToken = new JwtSecurityToken(
                issuer: jwtSettings.Issuer,
                audience: jwtSettings.Audience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(jwtSettings.DurationInMinutes),
                signingCredentials: signingCredentials);
            return jwtSecurityToken;
        }

        private RefreshToken GenerateRefreshToken(string ipAddress, int userId)
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.Now.AddDays(7),
                    Created = DateTime.Now,
                    CreatedByIp = ipAddress,
                    UserId = userId
                };
            }
        }


    }
}
