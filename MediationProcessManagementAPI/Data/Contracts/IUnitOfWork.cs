﻿using MediationProcessManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Contracts
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; }
        IPersonRepository Persons { get; }
        IMediatrRepository Mediatrs { get; }
        IRequestRepository Requests { get; }
        IAsyncRepository<Role> Roles { get; }
        IAsyncRepository<Office> Offices { get; }
        IAsyncRepository<RefreshToken> RefreshTokens { get; }
        IAsyncRepository<District> Districts { get; }
        IAsyncRepository<Court> Courts { get; }
        IAsyncRepository<DistrictCourt> DistrictCourts { get; }
        IAsyncRepository<DistrictMediatr> DistrictMediatrs { get; }
        IAsyncRepository<Profession> Professions { get; }
        IAsyncRepository<MediatrProfession> MediatrProfessions { get; }
        IAsyncRepository<Status> Statuses { get; }
        IAsyncRepository<SocialMedia> SocialMedias { get; }
        IAsyncRepository<Institution> Institutions { get; }


        Task SaveAllAsync();
    }
}
