﻿using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using MediationProcessManagementAPI.Models.Parameters;
using MediationProcessManagementAPI.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Contracts
{
    public interface IMediatrRepository : IAsyncRepository<Mediatr>
    {
        Task<IEnumerable<Mediatr>> GetMediatrWithProfessions(int? professionId);
        Task<IEnumerable<DistrictMediatrsDto>> GetDistrictMediatrsByProfessionAndCourt(MediatrSelectionRequestParameters reqParams);
        Task<int> GetMediatrIdByWorkload(string districtIds);
        Task<PagedResponse<MediatrTableDataDto>> GetPagedMediatrs(MediatrRequestParameters reqParams);
        Task<Mediatr> GetExtendedMediatrById(int mediatrId);
    }
}
