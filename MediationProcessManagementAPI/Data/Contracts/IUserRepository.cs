﻿using MediationProcessManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Contracts
{
    public interface IUserRepository : IAsyncRepository<User>
    {
    }
}
