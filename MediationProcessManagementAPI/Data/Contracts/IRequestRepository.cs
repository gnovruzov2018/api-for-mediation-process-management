﻿using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using MediationProcessManagementAPI.Models.Parameters;
using MediationProcessManagementAPI.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Contracts
{
    public interface IRequestRepository : IAsyncRepository<Request>
    {
        Task<Request> GetRequestById(int requestId);
        Task<PagedResponse<RequestReturnDto>> GetPagedRequestsForUsers(int userId, RequestParameters reqParams);
        Task<PagedResponse<RequestReturnDto>> GetPagedRequestsForMediatrs(int mediatrId, RequestParameters reqParams);
        Task<PagedResponse<RequestReturnDto>> GetPagedRequestsForOffices(int officeId, RequestParameters reqParams);
        Task<PagedResponse<RequestReturnDto>> GetPagedRequestsForAdmin(RequestParameters reqParams);
    }
}
