﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Contracts
{
	public interface IAsyncRepository<T> where T : class
	{
		Task<IReadOnlyList<T>> GetAllAsync();
		Task<IReadOnlyList<T>> GetAllAsync(Expression<Func<T, bool>> predicate);
		Task<IReadOnlyList<T>> GetAllAsync(Expression<Func<T, bool>> predicate = null,
										Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
										string includeString = null,
										bool disableTracking = true);
		Task<IReadOnlyList<T>> GetAllAsync(Expression<Func<T, bool>> predicate = null,
									   Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
									   List<Expression<Func<T, object>>> includes = null,
									   bool disableTracking = true);

		Task<T> GetOneAsync(Expression<Func<T, bool>> predicate);
		Task<T> GetOneAsync(Expression<Func<T, bool>> predicate = null,
										string includeString = null,
										bool disableTracking = true);
		Task<T> GetOneAsync(Expression<Func<T, bool>> predicate = null,
									   List<Expression<Func<T, object>>> includes = null,
									   bool disableTracking = true);
		Task<T> GetByIdAsync(int id);
		Task<T> AddAsync(T entity);
		Task UpdateAsync(T entity);
		Task DeleteAsync(T entity);
	}
}
