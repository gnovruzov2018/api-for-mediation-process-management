﻿using Dapper;
using MediationProcessManagementAPI.Helpers;
using MediationProcessManagementAPI.Data.Context;
using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MediationProcessManagementAPI.Wrappers;
using MediationProcessManagementAPI.Models.Parameters;
using AutoMapper;

namespace MediationProcessManagementAPI.Data.Repositories
{
    public class MediatrRepository : RepositoryBase<Mediatr>, IMediatrRepository
    {
        private readonly MpmDbContext context;
        private readonly IMapper mapper;

        public MediatrRepository(MpmDbContext context, IMapper mapper) : base(context)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<DistrictMediatrsDto>> GetDistrictMediatrsByProfessionAndCourt(MediatrSelectionRequestParameters reqParams)
        {
            var districtIds = string.Join(',', reqParams.DistrictIds);
            var procedureName = "spGetDistrictMediatrsByProfessionAndCourt";
            using (var connection = context.CreateConnection())
            {
                try
                {
                    var mediatrDictionary = new Dictionary<int, DistrictMediatrsDto>();
                    var districtMediatrs = await connection.QueryAsync<DistrictMediatrsDto, MediatrItemDto, DistrictMediatrsDto>
                        (procedureName, (district, mediatr) =>
                        {
                            DistrictMediatrsDto districtMediatr;

                            if (!mediatrDictionary.TryGetValue(district.DistrictId, out districtMediatr))
                            {
                                districtMediatr = district;
                                districtMediatr.Mediatrs = new List<MediatrItemDto>();
                                mediatrDictionary.Add(districtMediatr.DistrictId, districtMediatr);
                            }

                            districtMediatr.Mediatrs.Add(mediatr);
                            return districtMediatr;
                        },
                        splitOn: "MediatrId",
                        param: new { ProfessionId = reqParams.ProfessionId, CourtId = reqParams.CourtId, DistrictIds = districtIds },
                        commandType: CommandType.StoredProcedure);

                    foreach(var dm in districtMediatrs)
                    {
                        dm.Mediatrs.Shuffle<MediatrItemDto>();
                    }

                    return districtMediatrs.Distinct();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<Mediatr> GetExtendedMediatrById(int mediatrId)
        {
            var mediatr = await context.Mediatrs
                .Include(x => x.Office)
                .Include(x => x.Institution)
                .Include(x => x.MediatrProfessions)
                .ThenInclude(x => x.Profession)
                .Include(x => x.Certificates)
                .Include(x => x.Educations)
                .Include(x => x.DistrictMediatrs)
                .ThenInclude(x => x.District)
                .Include(x => x.MediatrsSocialMedias)
                .ThenInclude(x => x.SocialMedia)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == mediatrId);

            return mediatr;
        }

        public async Task<int> GetMediatrIdByWorkload(string districtIds)
        {
            var procedureName = "spGetMediatrIdByWorkload";
            using (var connection = context.CreateConnection())
            {
                try
                {
                    var mediatrId = await connection
                        .QuerySingleOrDefaultAsync<int>(procedureName, param: new { DistrictIds = districtIds }, commandType: CommandType.StoredProcedure);
                    return mediatrId;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<IEnumerable<Mediatr>> GetMediatrWithProfessions(int? professionId)
        {
            var query = context.Mediatrs.AsQueryable();

            if (professionId != null)
                query = query
                    .Where(x => x.MediatrProfessions.Any(c => c.ProfessionId == professionId.Value));

            var mediatrs = await query
                .Include(x => x.MediatrProfessions)
                .ThenInclude(x => x.Profession)
                .ToListAsync();

            return mediatrs;
        }

        public async Task<PagedResponse<MediatrTableDataDto>> GetPagedMediatrs(MediatrRequestParameters reqParams)
        {
            var query = context.Mediatrs.AsQueryable();

            var count = await query.AsNoTracking().CountAsync();

            var requests = await query.Skip((reqParams.PageNumber - 1) * reqParams.PageSize)
                .Take(reqParams.PageSize)
                .Include(x => x.DistrictMediatrs)
                .ThenInclude(x => x.District)
                .AsNoTracking()
                .ToListAsync();

            var mappedMediatrs = mapper.Map<IList<MediatrTableDataDto>>(requests);

            var pagedResponse = new PagedResponse<MediatrTableDataDto>
            {
                TotalCount = count,
                Data = mappedMediatrs,
                PageNumber = reqParams.PageNumber,
                PageSize = reqParams.PageSize
            };

            return pagedResponse;
        }
    }
}
