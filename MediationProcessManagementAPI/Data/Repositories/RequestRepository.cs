﻿using AutoMapper;
using MediationProcessManagementAPI.Data.Context;
using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using MediationProcessManagementAPI.Models.Parameters;
using MediationProcessManagementAPI.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Repositories
{
    public class RequestRepository : RepositoryBase<Request>, IRequestRepository
    {
        private readonly MpmDbContext context;
        private readonly IMapper mapper;

        public RequestRepository(MpmDbContext context, IMapper mapper) : base(context)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<Request> GetRequestById(int requestId)
        {
            var request = await context.Requests
                .Where(x => x.Id == requestId)
                .Include(x => x.Court)
                .Include(x => x.Mediatr)
                .Include(x => x.Profession)
                .Include(x => x.Status)
                .Include(x => x.User)
                .ThenInclude(x => x.Person)
                .Include(x => x.Office)
                .Include(x => x.Sides)
                .Include(x => x.RequestedMediatrs)
                .ThenInclude(x => x.Mediatr)
                .Include(x => x.RequestedDistricts)
                .ThenInclude(x => x.District)
                .FirstOrDefaultAsync();

            return request;
        }

        public async Task<PagedResponse<RequestReturnDto>> GetPagedRequestsForUsers(int userId, RequestParameters reqParams)
        {
            var query = context.Requests.Where(x => x.UserId == userId);

            var count = await query.AsNoTracking().CountAsync();

            query = query
                .Include(x => x.Court)
                .Include(x => x.Mediatr)
                .Include(x => x.Status)
                .Include(x => x.Office)
                .Include(x => x.User)
                .ThenInclude(x => x.Person)
                .Include(x => x.RequestedMediatrs)
                .ThenInclude(x => x.Mediatr)
                .AsQueryable();

            query = query.OrderByDescending(x => x.CreatedDate);

            var requests = await query
                .Skip((reqParams.PageNumber - 1) * reqParams.PageSize)
                .Take(reqParams.PageSize)
                .AsNoTracking()
                .ToListAsync();
                
            var mappedRequests = mapper.Map<IList<RequestReturnDto>>(requests);

            var pagedResponse = new PagedResponse<RequestReturnDto>
            {
                TotalCount = count,
                Data = mappedRequests,
                PageNumber = reqParams.PageNumber,
                PageSize = reqParams.PageSize
            };

            return pagedResponse;
        }

        public async Task<PagedResponse<RequestReturnDto>> GetPagedRequestsForMediatrs(int mediatrId, RequestParameters reqParams)
        {
            var query = context.Requests
                .Where(x => x.MediatrId == mediatrId || x.RequestedMediatrs.Any(rm => rm.MediatrId == mediatrId));

            var count = await query.AsNoTracking().CountAsync();

            query = query
                .Include(x => x.Court)
                .Include(x => x.Mediatr)
                .Include(x => x.Status)
                .Include(x => x.Office)
                .Include(x => x.User)
                .ThenInclude(x => x.Person)
                .Include(x => x.RequestedMediatrs)
                .ThenInclude(x => x.Mediatr)
                .AsQueryable();

            query = query.OrderByDescending(x => x.CreatedDate);

            var requests = await query
                .Skip((reqParams.PageNumber - 1) * reqParams.PageSize)
                .Take(reqParams.PageSize)
                .AsNoTracking()
                .ToListAsync();

            var mappedRequests = mapper.Map<IList<RequestReturnDto>>(requests);

            var pagedResponse = new PagedResponse<RequestReturnDto>
            {
                TotalCount = count,
                Data = mappedRequests,
                PageNumber = reqParams.PageNumber,
                PageSize = reqParams.PageSize
            };

            return pagedResponse;
        }

        public async Task<PagedResponse<RequestReturnDto>> GetPagedRequestsForOffices(int officeId, RequestParameters reqParams)
        {
            var query = context.Requests
                .Where(x => x.OfficeId == officeId);

            var count = await query.AsNoTracking().CountAsync();

            query = query
                .Include(x => x.Court)
                .Include(x => x.Mediatr)
                .Include(x => x.Status)
                .Include(x => x.Office)
                .Include(x => x.User)
                .ThenInclude(x => x.Person)
                .Include(x => x.RequestedMediatrs)
                .ThenInclude(x => x.Mediatr)
                .AsQueryable();

            query = query.OrderByDescending(x => x.CreatedDate);

            var requests = await query
                .Skip((reqParams.PageNumber - 1) * reqParams.PageSize)
                .Take(reqParams.PageSize)
                .AsNoTracking()
                .ToListAsync();

            var mappedRequests = mapper.Map<IList<RequestReturnDto>>(requests);

            var pagedResponse = new PagedResponse<RequestReturnDto>
            {
                TotalCount = count,
                Data = mappedRequests,
                PageNumber = reqParams.PageNumber,
                PageSize = reqParams.PageSize
            };

            return pagedResponse;
        }

        public async Task<PagedResponse<RequestReturnDto>> GetPagedRequestsForAdmin(RequestParameters reqParams)
        {
            var query = context.Requests.AsQueryable();

            var count = await query.AsNoTracking().CountAsync();

            query = query
                .Include(x => x.Court)
                .Include(x => x.Mediatr)
                .Include(x => x.Status)
                .Include(x => x.Office)
                .Include(x => x.User)
                .ThenInclude(x => x.Person)
                .Include(x => x.RequestedMediatrs)
                .ThenInclude(x => x.Mediatr)
                .AsQueryable();

            query = query.OrderByDescending(x => x.CreatedDate);

            var requests = await query
                .Skip((reqParams.PageNumber - 1) * reqParams.PageSize)
                .Take(reqParams.PageSize)
                .AsNoTracking()
                .ToListAsync();

            var mappedRequests = mapper.Map<IList<RequestReturnDto>>(requests);

            var pagedResponse = new PagedResponse<RequestReturnDto>
            {
                TotalCount = count,
                Data = mappedRequests,
                PageNumber = reqParams.PageNumber,
                PageSize = reqParams.PageSize
            };

            return pagedResponse;
        }
    }
}
