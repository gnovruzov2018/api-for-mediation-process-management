﻿using AutoMapper;
using MediationProcessManagementAPI.Data.Context;
using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using System;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MpmDbContext context;
        private readonly IMapper mapper;

        public UnitOfWork(MpmDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
            Users = new UserRepository(context);
            Persons = new PersonRepository(context);
            Roles = new RepositoryBase<Role>(context);
            Mediatrs = new MediatrRepository(context, mapper);
            Offices = new RepositoryBase<Office>(context);
            RefreshTokens = new RepositoryBase<RefreshToken>(context);
            Districts = new RepositoryBase<District>(context);
            Courts = new RepositoryBase<Court>(context);
            DistrictCourts = new RepositoryBase<DistrictCourt>(context);
            DistrictMediatrs = new RepositoryBase<DistrictMediatr>(context);
            Professions = new RepositoryBase<Profession>(context);
            MediatrProfessions = new RepositoryBase<MediatrProfession>(context);
            Statuses = new RepositoryBase<Status>(context);
            Requests = new RequestRepository(context, mapper);
            Institutions = new RepositoryBase<Institution>(context);
            SocialMedias = new RepositoryBase<SocialMedia>(context);
        }

        public IUserRepository Users { get; private set; }

        public IPersonRepository Persons { get; private set; }

        public IAsyncRepository<Role> Roles { get; private set; }

        public IMediatrRepository Mediatrs { get; private set; }
        public IRequestRepository Requests { get; private set; }

        public IAsyncRepository<Office> Offices { get; private set; }

        public IAsyncRepository<RefreshToken> RefreshTokens { get; private set; }

        public IAsyncRepository<District> Districts { get; private set; }

        public IAsyncRepository<Court> Courts { get; private set; }

        public IAsyncRepository<DistrictCourt> DistrictCourts { get; private set; }

        public IAsyncRepository<DistrictMediatr> DistrictMediatrs { get; private set; }

        public IAsyncRepository<Profession> Professions { get; private set; }

        public IAsyncRepository<MediatrProfession> MediatrProfessions { get; private set; }

        public IAsyncRepository<Status> Statuses { get; private set; }
        public IAsyncRepository<SocialMedia> SocialMedias { get; private set; }
        public IAsyncRepository<Institution> Institutions { get; private set; }

        public void Dispose(bool dispose)
        {
            if (dispose)
                context.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task SaveAllAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}
