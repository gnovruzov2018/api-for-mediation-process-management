﻿using MediationProcessManagementAPI.Data.Context;
using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Repositories
{
    public class PersonRepository : RepositoryBase<Person>, IPersonRepository
    {
        private readonly MpmDbContext context;

        public PersonRepository(MpmDbContext context) : base(context)
        {
            this.context = context;
        }
    }
}
