﻿using MediationProcessManagementAPI.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Context
{
    public class MpmDbContext : DbContext
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public MpmDbContext(DbContextOptions<MpmDbContext> options, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("ProdDbConnection");
        }

        public IDbConnection CreateConnection()
        => new SqlConnection(_connectionString);

        public DbSet<Role> Roles { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Mediatr> Mediatrs { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Court> Courts { get; set; }
        public DbSet<Institution> Institutions { get; set; }
        public DbSet<DistrictCourt> DistrictCourts { get; set; }
        public DbSet<DistrictMediatr> DistrictMediatrs { get; set; }
        public DbSet<Profession> Professions { get; set; }
        public DbSet<MediatrProfession> MediatrProfessions { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Side> Sides { get; set; }
        public DbSet<RequestedDistrict> RequestedDistricts { get; set; }
        public DbSet<RequestedMediatr> RequestedMediatrs { get; set; }
        public DbSet<SocialMedia> SocialMedias { get; set; }
        public DbSet<MediatrSocialMedia> MediatrSocialMedias { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<Certificate> Certificates { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>()
                .Property(x => x.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<District>()
                .Property(x => x.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<Court>()
                .Property(x => x.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<Profession>()
               .Property(x => x.Id)
               .ValueGeneratedNever();

            modelBuilder.Entity<Status>()
               .Property(x => x.Id)
               .ValueGeneratedNever();

            modelBuilder.Entity<SocialMedia>()
               .Property(x => x.Id)
               .ValueGeneratedNever();

            modelBuilder.Entity<DistrictCourt>()
                .HasKey(sc => new { sc.DistrictId, sc.CourtId });

            modelBuilder.Entity<DistrictMediatr>()
                .HasKey(sc => new { sc.DistrictId, sc.MediatrId });

            modelBuilder.Entity<MediatrProfession>()
                .HasKey(sc => new { sc.ProfessionId, sc.MediatrId });

            modelBuilder.Entity<RequestedDistrict>()
                .HasKey(sc => new { sc.RequestId, sc.DistrictId });

            modelBuilder.Entity<RequestedMediatr>()
                .HasKey(sc => new { sc.RequestId, sc.MediatrId });

            modelBuilder.Entity<MediatrSocialMedia>()
                .HasKey(sc => new { sc.SocialMediaId, sc.MediatrId });
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var entry in ChangeTracker.Entries<EntityBase>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedDate = DateTime.Now;
                        break;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
