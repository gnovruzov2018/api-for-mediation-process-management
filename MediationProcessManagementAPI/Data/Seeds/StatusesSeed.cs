﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public class StatusesSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Statuses
            var statuses = await unitOfWork.Statuses.GetAllAsync();

            if (!statuses.Any())
            {
                await unitOfWork.Statuses.AddAsync(new Status { Id = 1, StatusName = "Yeni" });
                await unitOfWork.Statuses.AddAsync(new Status { Id = 2, StatusName = "Qəbul edilmiş" });
                await unitOfWork.Statuses.AddAsync(new Status { Id = 3, StatusName = "İmtina olunmuş" });
                await unitOfWork.Statuses.AddAsync(new Status { Id = 4, StatusName = "Vaxtı bitmiş" });
                await unitOfWork.Statuses.AddAsync(new Status { Id = 5, StatusName = "Təsdiq edilmiş" });
            }
        }
    }
}
