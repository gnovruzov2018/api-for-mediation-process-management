﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class SocialMediasSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Social Medias
            var data = await unitOfWork.SocialMedias.GetAllAsync();

            if (!data.Any())
            {
                await unitOfWork.SocialMedias.AddAsync(new SocialMedia { Id = 1, SocialMediaName = "Facebook" });
                await unitOfWork.SocialMedias.AddAsync(new SocialMedia { Id = 2, SocialMediaName = "Instagram" });
                await unitOfWork.SocialMedias.AddAsync(new SocialMedia { Id = 3, SocialMediaName = "Twitter" });
                await unitOfWork.SocialMedias.AddAsync(new SocialMedia { Id = 4, SocialMediaName = "Linkedin" });
            }
        }
    }
}
