﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class DistrictMediatrsSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Districts - Mediatrs
            var data = await unitOfWork.DistrictMediatrs.GetAllAsync();

            if (!data.Any())
            {
                string filePath = @"wwwroot/json/districts-mediatrs.json";

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    List<DistrictMediatr> list = JsonConvert.DeserializeObject<List<DistrictMediatr>>(json);

                    foreach (var item in list)
                    {
                        await unitOfWork.DistrictMediatrs.AddAsync(item);
                    }
                }
            }
        }
    }
}
