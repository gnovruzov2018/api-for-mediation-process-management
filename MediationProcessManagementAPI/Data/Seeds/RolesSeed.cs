﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class RolesSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Roles
            var roles = await unitOfWork.Roles.GetAllAsync();

            if (!roles.Any())
            {
                await unitOfWork.Roles.AddAsync(new Role { Id = 1, RoleName = "Admin", RoleDesc = "Administrator" });
                await unitOfWork.Roles.AddAsync(new Role { Id = 2, RoleName = "Council", RoleDesc = "Mediasiya Şurası" });
                await unitOfWork.Roles.AddAsync(new Role { Id = 3, RoleName = "Office", RoleDesc = "Mediasiya təşkilatı" });
                await unitOfWork.Roles.AddAsync(new Role { Id = 4, RoleName = "Mediatr", RoleDesc = "Fərdi mediator" });
                await unitOfWork.Roles.AddAsync(new Role { Id = 5, RoleName = "Citizen", RoleDesc = "Vətəndaş" });
            }
        }
    }
}
