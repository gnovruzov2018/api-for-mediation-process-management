﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class DistrictCourtsSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Courts - Districts
            var data = await unitOfWork.DistrictCourts.GetAllAsync();

            if (!data.Any())
            {
                string filePath = @"wwwroot/json/districts-courts.json";

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    List<DistrictCourt> list = JsonConvert.DeserializeObject<List<DistrictCourt>>(json);

                    foreach(var item in list)
                    {
                        await unitOfWork.DistrictCourts.AddAsync(item);
                    }
                }
            }
        }
    }
}
