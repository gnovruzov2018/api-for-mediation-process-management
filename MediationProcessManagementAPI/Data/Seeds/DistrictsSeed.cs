﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class DistrictsSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Districts
            var districts = await unitOfWork.Districts.GetAllAsync();

            if (!districts.Any())
            {
                string filePath = @"wwwroot/json/districts.json";

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    List<District> list = JsonConvert.DeserializeObject<List<District>>(json);

                    foreach(var item in list)
                    {
                        char padChar = '0';
                        if(item.DistrictCode.Length < 3)
                        {
                            item.DistrictCode = string.Empty.PadLeft((3 - item.DistrictCode.Length), padChar) + item.DistrictCode;
                        }

                        await unitOfWork.Districts.AddAsync(item);
                    }
                }
            }

        }
    }
}
