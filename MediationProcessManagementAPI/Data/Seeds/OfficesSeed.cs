﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class OfficesSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Offices
            var offices = await unitOfWork.Offices.GetAllAsync();

            if (!offices.Any())
            {
                await unitOfWork.Offices.AddAsync(new Office { OfficeName = "Mediasiya Təşkilatı 1" });
                await unitOfWork.Offices.AddAsync(new Office { OfficeName = "Mediasiya Təşkilatı 2" });
                await unitOfWork.Offices.AddAsync(new Office { OfficeName = "Mediasiya Təşkilatı 3" });
            }
        }
    }
}
