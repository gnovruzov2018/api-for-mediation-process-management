﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class MediatrProfessionsSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Mediatrs - Professions
            var data = await unitOfWork.MediatrProfessions.GetAllAsync();

            if (!data.Any())
            {
                string filePath = @"wwwroot/json/mediatrs-professions.json";

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    List<MediatrProfession> list = JsonConvert.DeserializeObject<List<MediatrProfession>>(json);

                    foreach (var item in list)
                    {
                        await unitOfWork.MediatrProfessions.AddAsync(item);
                    }
                }
            }
        }
    }
}
