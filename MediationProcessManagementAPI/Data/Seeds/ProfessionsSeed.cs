﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class ProfessionsSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Professions
            var professions = await unitOfWork.Professions.GetAllAsync();

            if (!professions.Any())
            {
                await unitOfWork.Professions.AddAsync(new Profession { Id = 1, ProfessionName = "Ümümi (o cümlədən mülki, istehlakçı, inzibati və s.)" });
                await unitOfWork.Professions.AddAsync(new Profession { Id = 2, ProfessionName = "Kommersiya münasibətləri üzrə" });
                await unitOfWork.Professions.AddAsync(new Profession { Id = 3, ProfessionName = "Ailə münasibətləri üzrə" });
                await unitOfWork.Professions.AddAsync(new Profession { Id = 4, ProfessionName = "Əmək münasibətləri üzrə" });
            }
        }
    }
}
