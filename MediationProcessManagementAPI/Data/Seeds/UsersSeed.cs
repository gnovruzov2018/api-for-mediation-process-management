﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class UsersSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Users
            var users = await unitOfWork.Users.GetAllAsync();

            if (!users.Any())
            {
                //await unitOfWork.Users.AddAsync(new User { Login = "mediatr1", Password = "mediatr1", MediatrId = 1, RoleId = 4});
                //await unitOfWork.Users.AddAsync(new User { Login = "mediatr2", Password = "mediatr2", MediatrId = 2, RoleId = 4 });
                await unitOfWork.Users.AddAsync(new User { Login = "council", Password = "council", OfficeId = 1, RoleId = 2 });
                await unitOfWork.Users.AddAsync(new User { Login = "office", Password = "office", OfficeId = 2, RoleId = 3 });
            }
        }
    }
}
