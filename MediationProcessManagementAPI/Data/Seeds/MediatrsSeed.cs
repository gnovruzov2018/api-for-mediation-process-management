﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class MediatrsSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            // Seed Mediatrs
            var mediatrs = await unitOfWork.Mediatrs.GetAllAsync();

            if (!mediatrs.Any())
            {
                string filePath = @"wwwroot/json/mediatrs.json";

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    List<Mediatr> list = JsonConvert.DeserializeObject<List<Mediatr>>(json);

                    foreach (var item in list)
                    {
                        if(item.DocNumber[1] == 'A')
                        {
                            item.DocSeries = "AA";
                            item.DocNumber = item.DocNumber.Substring(2);
                        }
                        else
                        {
                            item.DocSeries = "AZE";
                            item.DocNumber = item.DocNumber.Substring(3);
                        }
                        await unitOfWork.Mediatrs.AddAsync(item);
                    }
                }
            }

        }
    }
}
