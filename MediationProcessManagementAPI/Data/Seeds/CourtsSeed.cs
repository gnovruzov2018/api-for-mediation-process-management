﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class CourtsSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Courts
            var courts = await unitOfWork.Courts.GetAllAsync();

            if (!courts.Any())
            {
                string filePath = @"wwwroot/json/courts.json";

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    List<Court> list = JsonConvert.DeserializeObject<List<Court>>(json);

                    foreach (var item in list)
                    {
                        await unitOfWork.Courts.AddAsync(item);
                    }
                }
            }

        }
    }
}
