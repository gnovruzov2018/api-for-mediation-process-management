﻿using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Data.Seeds
{
    public static class InstitutionsSeed
    {
        public static async Task SeedAsync(IUnitOfWork unitOfWork)
        {
            //Seed Institutions
            var institutions = await unitOfWork.Institutions.GetAllAsync();

            if (!institutions.Any())
            {
                await unitOfWork.Institutions.AddAsync(new Institution { InstitutionName = "	Azərbaycan Respublikasının Ədliyyə Nazirliyinin Ədliyyə Akademiyası" });
            }
        }
    }
}
