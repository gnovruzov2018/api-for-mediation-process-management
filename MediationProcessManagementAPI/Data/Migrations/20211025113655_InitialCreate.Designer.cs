﻿// <auto-generated />
using System;
using MediationProcessManagementAPI.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MediationProcessManagementAPI.Data.Migrations
{
    [DbContext(typeof(MpmDbContext))]
    [Migration("20211025113655_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.19")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Certificate", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CertificateName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CertificationDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("IssuerInstitution")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("MediatrId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("MediatrId");

                    b.ToTable("Certificates");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Court", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<string>("CourtName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("CourtType")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Courts");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.District", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<string>("DistrictCode")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("DistrictName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("ParentId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ParentId");

                    b.ToTable("Districts");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.DistrictCourt", b =>
                {
                    b.Property<int>("DistrictId")
                        .HasColumnType("int");

                    b.Property<int>("CourtId")
                        .HasColumnType("int");

                    b.HasKey("DistrictId", "CourtId");

                    b.HasIndex("CourtId");

                    b.ToTable("DistrictCourts");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.DistrictMediatr", b =>
                {
                    b.Property<int>("DistrictId")
                        .HasColumnType("int");

                    b.Property<int>("MediatrId")
                        .HasColumnType("int");

                    b.HasKey("DistrictId", "MediatrId");

                    b.HasIndex("MediatrId");

                    b.ToTable("DistrictMediatrs");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Education", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CollegeName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("MajorName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("MediatrId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("MediatrId");

                    b.ToTable("Educations");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Institution", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("InstitutionName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Institutions");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Mediatr", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ActingAddress")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("DateOfBirth")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("DocNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("DocSeries")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("InstitutionId")
                        .HasColumnType("int");

                    b.Property<string>("LanguageSkills")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("MembershipDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("MiddleName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("OfficeId")
                        .HasColumnType("int");

                    b.Property<string>("OtherPosition")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("OtherWorkplace")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PIN")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Phone")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RegAddress")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RegistryNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("VOEN")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("InstitutionId");

                    b.HasIndex("OfficeId");

                    b.ToTable("Mediatrs");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.MediatrProfession", b =>
                {
                    b.Property<int>("ProfessionId")
                        .HasColumnType("int");

                    b.Property<int>("MediatrId")
                        .HasColumnType("int");

                    b.HasKey("ProfessionId", "MediatrId");

                    b.HasIndex("MediatrId");

                    b.ToTable("MediatrProfessions");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.MediatrSocialMedia", b =>
                {
                    b.Property<int>("SocialMediaId")
                        .HasColumnType("int");

                    b.Property<int>("MediatrId")
                        .HasColumnType("int");

                    b.HasKey("SocialMediaId", "MediatrId");

                    b.HasIndex("MediatrId");

                    b.ToTable("MediatrSocialMedias");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Office", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("OfficeName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Phone")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Offices");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Person", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("CurrentAddress")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("DateOfBirth")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("DocNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("DocSeries")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Gender")
                        .HasColumnType("int");

                    b.Property<string>("Image")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("MiddleName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PIN")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Phone")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RegAddress")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Persons");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Profession", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<string>("ProfessionDesc")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ProfessionName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Professions");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.RefreshToken", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<string>("CreatedByIp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Expires")
                        .HasColumnType("datetime2");

                    b.Property<string>("ReplacedByToken")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("Revoked")
                        .HasColumnType("datetime2");

                    b.Property<string>("RevokedByIp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Token")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("RefreshTokens");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Request", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("AcceptDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("CaseInAction")
                        .HasColumnType("bit");

                    b.Property<string>("ConflictInfo")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("CourtCaseInfo")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("CourtId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<int?>("MediatrId")
                        .HasColumnType("int");

                    b.Property<bool>("MediatrSelected")
                        .HasColumnType("bit");

                    b.Property<int?>("OfficeId")
                        .HasColumnType("int");

                    b.Property<string>("PrefferedSessionTime")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ProfessionId")
                        .HasColumnType("int");

                    b.Property<string>("RequiredLangs")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("StatusId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CourtId");

                    b.HasIndex("MediatrId");

                    b.HasIndex("OfficeId");

                    b.HasIndex("ProfessionId");

                    b.HasIndex("StatusId");

                    b.HasIndex("UserId");

                    b.ToTable("Requests");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.RequestedDistrict", b =>
                {
                    b.Property<int>("RequestId")
                        .HasColumnType("int");

                    b.Property<int>("DistrictId")
                        .HasColumnType("int");

                    b.HasKey("RequestId", "DistrictId");

                    b.HasIndex("DistrictId");

                    b.ToTable("RequestedDistricts");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.RequestedMediatr", b =>
                {
                    b.Property<int>("RequestId")
                        .HasColumnType("int");

                    b.Property<int>("MediatrId")
                        .HasColumnType("int");

                    b.Property<DateTime?>("DeclineDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("MediatrStatus")
                        .HasColumnType("int");

                    b.Property<string>("RejectText")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("RequestId", "MediatrId");

                    b.HasIndex("MediatrId");

                    b.ToTable("RequestedMediatrs");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Role", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<string>("RoleDesc")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RoleName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Side", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("AdvocateFirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("AdvocateLastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("AdvocateMiddleName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("OrganizationName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Phone")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("RequestId")
                        .HasColumnType("int");

                    b.Property<string>("SideFirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("SideGender")
                        .HasColumnType("int");

                    b.Property<string>("SideLastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("SideMiddleName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("RequestId");

                    b.ToTable("Sides");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.SocialMedia", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<string>("SocialMediaName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("SocialMedias");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Status", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<string>("StatusName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Statuses");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Login")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("MediatrId")
                        .HasColumnType("int");

                    b.Property<int?>("OfficeId")
                        .HasColumnType("int");

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("PersonId")
                        .HasColumnType("int");

                    b.Property<int>("RoleId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("MediatrId");

                    b.HasIndex("OfficeId");

                    b.HasIndex("PersonId")
                        .IsUnique()
                        .HasFilter("[PersonId] IS NOT NULL");

                    b.HasIndex("RoleId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Certificate", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Mediatr", "Mediatr")
                        .WithMany("Certificates")
                        .HasForeignKey("MediatrId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.District", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.District", "ParentDistrict")
                        .WithMany("ChildDistricts")
                        .HasForeignKey("ParentId");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.DistrictCourt", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Court", "Court")
                        .WithMany("DistrictCourts")
                        .HasForeignKey("CourtId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MediationProcessManagementAPI.Models.District", "District")
                        .WithMany("DistrictCourts")
                        .HasForeignKey("DistrictId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.DistrictMediatr", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.District", "District")
                        .WithMany()
                        .HasForeignKey("DistrictId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MediationProcessManagementAPI.Models.Mediatr", "Mediatr")
                        .WithMany()
                        .HasForeignKey("MediatrId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Education", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Mediatr", "Mediatr")
                        .WithMany("Educations")
                        .HasForeignKey("MediatrId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Mediatr", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Institution", "Institution")
                        .WithMany()
                        .HasForeignKey("InstitutionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MediationProcessManagementAPI.Models.Office", "Office")
                        .WithMany("Mediatrs")
                        .HasForeignKey("OfficeId");
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.MediatrProfession", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Mediatr", "Mediatr")
                        .WithMany("MediatrProfessions")
                        .HasForeignKey("MediatrId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MediationProcessManagementAPI.Models.Profession", "Profession")
                        .WithMany("MediatrProfessions")
                        .HasForeignKey("ProfessionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.MediatrSocialMedia", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Mediatr", "Mediatr")
                        .WithMany("MediatrsSocialMedias")
                        .HasForeignKey("MediatrId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MediationProcessManagementAPI.Models.SocialMedia", "SocialMedia")
                        .WithMany()
                        .HasForeignKey("SocialMediaId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.RefreshToken", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.User", "User")
                        .WithMany("RefreshTokens")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Request", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Court", "Court")
                        .WithMany()
                        .HasForeignKey("CourtId");

                    b.HasOne("MediationProcessManagementAPI.Models.Mediatr", "Mediatr")
                        .WithMany()
                        .HasForeignKey("MediatrId");

                    b.HasOne("MediationProcessManagementAPI.Models.Office", "Office")
                        .WithMany()
                        .HasForeignKey("OfficeId");

                    b.HasOne("MediationProcessManagementAPI.Models.Profession", "Profession")
                        .WithMany()
                        .HasForeignKey("ProfessionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MediationProcessManagementAPI.Models.Status", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MediationProcessManagementAPI.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.RequestedDistrict", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.District", "District")
                        .WithMany()
                        .HasForeignKey("DistrictId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MediationProcessManagementAPI.Models.Request", "Request")
                        .WithMany("RequestedDistricts")
                        .HasForeignKey("RequestId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.RequestedMediatr", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Mediatr", "Mediatr")
                        .WithMany()
                        .HasForeignKey("MediatrId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MediationProcessManagementAPI.Models.Request", "Request")
                        .WithMany("RequestedMediatrs")
                        .HasForeignKey("RequestId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.Side", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Request", "Request")
                        .WithMany("Sides")
                        .HasForeignKey("RequestId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MediationProcessManagementAPI.Models.User", b =>
                {
                    b.HasOne("MediationProcessManagementAPI.Models.Mediatr", "Mediatr")
                        .WithMany()
                        .HasForeignKey("MediatrId");

                    b.HasOne("MediationProcessManagementAPI.Models.Office", "Office")
                        .WithMany()
                        .HasForeignKey("OfficeId");

                    b.HasOne("MediationProcessManagementAPI.Models.Person", "Person")
                        .WithOne("User")
                        .HasForeignKey("MediationProcessManagementAPI.Models.User", "PersonId");

                    b.HasOne("MediationProcessManagementAPI.Models.Role", "Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
