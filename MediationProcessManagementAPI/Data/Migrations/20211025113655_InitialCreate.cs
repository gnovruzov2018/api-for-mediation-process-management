﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MediationProcessManagementAPI.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Courts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    CourtName = table.Column<string>(nullable: true),
                    CourtType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    DistrictName = table.Column<string>(nullable: true),
                    DistrictCode = table.Column<string>(nullable: true),
                    ParentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Districts_Districts_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Institutions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    InstitutionName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Institutions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Offices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    OfficeName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<string>(nullable: true),
                    PIN = table.Column<string>(nullable: true),
                    DocSeries = table.Column<string>(nullable: true),
                    DocNumber = table.Column<string>(nullable: true),
                    RegAddress = table.Column<string>(nullable: true),
                    CurrentAddress = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Professions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    ProfessionName = table.Column<string>(nullable: true),
                    ProfessionDesc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    RoleName = table.Column<string>(nullable: true),
                    RoleDesc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SocialMedias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    SocialMediaName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SocialMedias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    StatusName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DistrictCourts",
                columns: table => new
                {
                    DistrictId = table.Column<int>(nullable: false),
                    CourtId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistrictCourts", x => new { x.DistrictId, x.CourtId });
                    table.ForeignKey(
                        name: "FK_DistrictCourts_Courts_CourtId",
                        column: x => x.CourtId,
                        principalTable: "Courts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DistrictCourts_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Mediatrs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<string>(nullable: true),
                    PIN = table.Column<string>(nullable: true),
                    DocSeries = table.Column<string>(nullable: true),
                    DocNumber = table.Column<string>(nullable: true),
                    RegAddress = table.Column<string>(nullable: true),
                    ActingAddress = table.Column<string>(nullable: true),
                    MembershipDate = table.Column<DateTime>(nullable: false),
                    LanguageSkills = table.Column<string>(nullable: true),
                    VOEN = table.Column<string>(nullable: true),
                    RegistryNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    OtherWorkplace = table.Column<string>(nullable: true),
                    OtherPosition = table.Column<string>(nullable: true),
                    OfficeId = table.Column<int>(nullable: true),
                    InstitutionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mediatrs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Mediatrs_Institutions_InstitutionId",
                        column: x => x.InstitutionId,
                        principalTable: "Institutions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Mediatrs_Offices_OfficeId",
                        column: x => x.OfficeId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Certificates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    IssuerInstitution = table.Column<string>(nullable: true),
                    CertificateName = table.Column<string>(nullable: true),
                    CertificationDate = table.Column<DateTime>(nullable: false),
                    MediatrId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Certificates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Certificates_Mediatrs_MediatrId",
                        column: x => x.MediatrId,
                        principalTable: "Mediatrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DistrictMediatrs",
                columns: table => new
                {
                    DistrictId = table.Column<int>(nullable: false),
                    MediatrId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistrictMediatrs", x => new { x.DistrictId, x.MediatrId });
                    table.ForeignKey(
                        name: "FK_DistrictMediatrs_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DistrictMediatrs_Mediatrs_MediatrId",
                        column: x => x.MediatrId,
                        principalTable: "Mediatrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Educations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CollegeName = table.Column<string>(nullable: true),
                    MajorName = table.Column<string>(nullable: true),
                    MediatrId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Educations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Educations_Mediatrs_MediatrId",
                        column: x => x.MediatrId,
                        principalTable: "Mediatrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MediatrProfessions",
                columns: table => new
                {
                    MediatrId = table.Column<int>(nullable: false),
                    ProfessionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MediatrProfessions", x => new { x.ProfessionId, x.MediatrId });
                    table.ForeignKey(
                        name: "FK_MediatrProfessions_Mediatrs_MediatrId",
                        column: x => x.MediatrId,
                        principalTable: "Mediatrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MediatrProfessions_Professions_ProfessionId",
                        column: x => x.ProfessionId,
                        principalTable: "Professions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MediatrSocialMedias",
                columns: table => new
                {
                    SocialMediaId = table.Column<int>(nullable: false),
                    MediatrId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MediatrSocialMedias", x => new { x.SocialMediaId, x.MediatrId });
                    table.ForeignKey(
                        name: "FK_MediatrSocialMedias_Mediatrs_MediatrId",
                        column: x => x.MediatrId,
                        principalTable: "Mediatrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MediatrSocialMedias_SocialMedias_SocialMediaId",
                        column: x => x.SocialMediaId,
                        principalTable: "SocialMedias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Login = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    OfficeId = table.Column<int>(nullable: true),
                    MediatrId = table.Column<int>(nullable: true),
                    PersonId = table.Column<int>(nullable: true),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Mediatrs_MediatrId",
                        column: x => x.MediatrId,
                        principalTable: "Mediatrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Offices_OfficeId",
                        column: x => x.OfficeId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RefreshTokens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Token = table.Column<string>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    CreatedByIp = table.Column<string>(nullable: true),
                    Revoked = table.Column<DateTime>(nullable: true),
                    RevokedByIp = table.Column<string>(nullable: true),
                    ReplacedByToken = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RefreshTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Requests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ConflictInfo = table.Column<string>(nullable: true),
                    CourtCaseInfo = table.Column<string>(nullable: true),
                    PrefferedSessionTime = table.Column<string>(nullable: true),
                    RequiredLangs = table.Column<string>(nullable: true),
                    CaseInAction = table.Column<bool>(nullable: false),
                    MediatrSelected = table.Column<bool>(nullable: false),
                    AcceptDate = table.Column<DateTime>(nullable: true),
                    OfficeId = table.Column<int>(nullable: true),
                    CourtId = table.Column<int>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    MediatrId = table.Column<int>(nullable: true),
                    StatusId = table.Column<int>(nullable: false),
                    ProfessionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Requests_Courts_CourtId",
                        column: x => x.CourtId,
                        principalTable: "Courts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Requests_Mediatrs_MediatrId",
                        column: x => x.MediatrId,
                        principalTable: "Mediatrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Requests_Offices_OfficeId",
                        column: x => x.OfficeId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Requests_Professions_ProfessionId",
                        column: x => x.ProfessionId,
                        principalTable: "Professions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Requests_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Requests_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RequestedDistricts",
                columns: table => new
                {
                    RequestId = table.Column<int>(nullable: false),
                    DistrictId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestedDistricts", x => new { x.RequestId, x.DistrictId });
                    table.ForeignKey(
                        name: "FK_RequestedDistricts_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RequestedDistricts_Requests_RequestId",
                        column: x => x.RequestId,
                        principalTable: "Requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RequestedMediatrs",
                columns: table => new
                {
                    RequestId = table.Column<int>(nullable: false),
                    MediatrId = table.Column<int>(nullable: false),
                    MediatrStatus = table.Column<int>(nullable: false),
                    DeclineDate = table.Column<DateTime>(nullable: true),
                    RejectText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestedMediatrs", x => new { x.RequestId, x.MediatrId });
                    table.ForeignKey(
                        name: "FK_RequestedMediatrs_Mediatrs_MediatrId",
                        column: x => x.MediatrId,
                        principalTable: "Mediatrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RequestedMediatrs_Requests_RequestId",
                        column: x => x.RequestId,
                        principalTable: "Requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sides",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    SideFirstName = table.Column<string>(nullable: true),
                    SideLastName = table.Column<string>(nullable: true),
                    SideMiddleName = table.Column<string>(nullable: true),
                    SideGender = table.Column<int>(nullable: false),
                    AdvocateFirstName = table.Column<string>(nullable: true),
                    AdvocateLastName = table.Column<string>(nullable: true),
                    AdvocateMiddleName = table.Column<string>(nullable: true),
                    OrganizationName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    RequestId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sides", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sides_Requests_RequestId",
                        column: x => x.RequestId,
                        principalTable: "Requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Certificates_MediatrId",
                table: "Certificates",
                column: "MediatrId");

            migrationBuilder.CreateIndex(
                name: "IX_DistrictCourts_CourtId",
                table: "DistrictCourts",
                column: "CourtId");

            migrationBuilder.CreateIndex(
                name: "IX_DistrictMediatrs_MediatrId",
                table: "DistrictMediatrs",
                column: "MediatrId");

            migrationBuilder.CreateIndex(
                name: "IX_Districts_ParentId",
                table: "Districts",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Educations_MediatrId",
                table: "Educations",
                column: "MediatrId");

            migrationBuilder.CreateIndex(
                name: "IX_MediatrProfessions_MediatrId",
                table: "MediatrProfessions",
                column: "MediatrId");

            migrationBuilder.CreateIndex(
                name: "IX_Mediatrs_InstitutionId",
                table: "Mediatrs",
                column: "InstitutionId");

            migrationBuilder.CreateIndex(
                name: "IX_Mediatrs_OfficeId",
                table: "Mediatrs",
                column: "OfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_MediatrSocialMedias_MediatrId",
                table: "MediatrSocialMedias",
                column: "MediatrId");

            migrationBuilder.CreateIndex(
                name: "IX_RefreshTokens_UserId",
                table: "RefreshTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestedDistricts_DistrictId",
                table: "RequestedDistricts",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestedMediatrs_MediatrId",
                table: "RequestedMediatrs",
                column: "MediatrId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_CourtId",
                table: "Requests",
                column: "CourtId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_MediatrId",
                table: "Requests",
                column: "MediatrId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_OfficeId",
                table: "Requests",
                column: "OfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_ProfessionId",
                table: "Requests",
                column: "ProfessionId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_StatusId",
                table: "Requests",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_UserId",
                table: "Requests",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Sides_RequestId",
                table: "Sides",
                column: "RequestId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_MediatrId",
                table: "Users",
                column: "MediatrId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_OfficeId",
                table: "Users",
                column: "OfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_PersonId",
                table: "Users",
                column: "PersonId",
                unique: true,
                filter: "[PersonId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Certificates");

            migrationBuilder.DropTable(
                name: "DistrictCourts");

            migrationBuilder.DropTable(
                name: "DistrictMediatrs");

            migrationBuilder.DropTable(
                name: "Educations");

            migrationBuilder.DropTable(
                name: "MediatrProfessions");

            migrationBuilder.DropTable(
                name: "MediatrSocialMedias");

            migrationBuilder.DropTable(
                name: "RefreshTokens");

            migrationBuilder.DropTable(
                name: "RequestedDistricts");

            migrationBuilder.DropTable(
                name: "RequestedMediatrs");

            migrationBuilder.DropTable(
                name: "Sides");

            migrationBuilder.DropTable(
                name: "SocialMedias");

            migrationBuilder.DropTable(
                name: "Districts");

            migrationBuilder.DropTable(
                name: "Requests");

            migrationBuilder.DropTable(
                name: "Courts");

            migrationBuilder.DropTable(
                name: "Professions");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Mediatrs");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Institutions");

            migrationBuilder.DropTable(
                name: "Offices");
        }
    }
}
