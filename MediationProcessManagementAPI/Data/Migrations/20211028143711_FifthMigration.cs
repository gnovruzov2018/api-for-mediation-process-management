﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MediationProcessManagementAPI.Data.Migrations
{
    public partial class FifthMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "Mediatrs");

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "Mediatrs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "Mediatrs");

            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "Mediatrs",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
