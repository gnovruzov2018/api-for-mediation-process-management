﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MediationProcessManagementAPI.Data.Migrations
{
    public partial class SecondMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LinkToPage",
                table: "MediatrSocialMedias",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "Mediatrs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalPageLink",
                table: "Mediatrs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "GraduationDate",
                table: "Educations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LinkToPage",
                table: "MediatrSocialMedias");

            migrationBuilder.DropColumn(
                name: "Image",
                table: "Mediatrs");

            migrationBuilder.DropColumn(
                name: "PersonalPageLink",
                table: "Mediatrs");

            migrationBuilder.DropColumn(
                name: "GraduationDate",
                table: "Educations");
        }
    }
}
