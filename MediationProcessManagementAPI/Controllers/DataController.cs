﻿using AutoMapper;
using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Helpers;
using MediationProcessManagementAPI.Interfaces;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using MediationProcessManagementAPI.Models.Enums;
using MediationProcessManagementAPI.Models.Errors;
using MediationProcessManagementAPI.Models.Parameters;
using MediationProcessManagementAPI.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class DataController : ControllerBase
    {
        private readonly IRegistryService registryService;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IWebHostEnvironment hostingEnvironment;

        public DataController(IRegistryService registryService,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IWebHostEnvironment hostingEnvironment)
        {
            this.registryService = registryService ?? throw new ArgumentNullException(nameof(registryService));
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
        }

        [HttpGet("districts")]
        [ProducesResponseType(typeof(Response<IEnumerable<District>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<District>>>> GetDistricts(bool includeChilds)
        {
            List<Expression<Func<District, object>>> includeList = new List<Expression<Func<District, object>>>();
            includeList.Add(x => x.ChildDistricts);

            IReadOnlyList<District> districts = null;
            if (!includeChilds)
                districts = await unitOfWork.Districts.GetAllAsync(x => x.ParentId == null);
            else
                districts = await unitOfWork.Districts.GetAllAsync(x => x.ParentId == null, includes: includeList);

            return Ok(new Response<IEnumerable<District>>(districts));
        }

        [HttpGet("child-districts")]
        [ProducesResponseType(typeof(Response<IEnumerable<District>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<District>>>> GetChildDistricts()
        {
            var districts = await unitOfWork.Districts.GetAllAsync(x => x.Id != 10 && x.Id != 31);

            return Ok(new Response<IEnumerable<District>>(districts));
        }

        [HttpGet("courts")]
        [ProducesResponseType(typeof(Response<IEnumerable<Court>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<Court>>>> GetCourts()
        {
            var courts = await unitOfWork.Courts.GetAllAsync(x => x.CourtType == CourtType.District);
            return Ok(new Response<IEnumerable<Court>>(courts));
        }

        [HttpGet("districts/courts/{districtId}")]
        [ProducesResponseType(typeof(Response<IEnumerable<Court>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<Court>>>> GetCourts([FromRoute]int districtId, [FromQuery] int? courtType)
        {
            IReadOnlyList<Court> courts = null;
            var courtIds = await unitOfWork.DistrictCourts.GetAllAsync(x => x.DistrictId == districtId);
            if (courtType != null)
                courts = await unitOfWork.Courts.GetAllAsync(x => x.CourtType == (CourtType)courtType && courtIds.Select(c => c.CourtId).Contains(x.Id));
            else
                courts = await unitOfWork.Courts.GetAllAsync(x => courtIds.Select(c => c.CourtId).Contains(x.Id));

            return Ok(new Response<IEnumerable<Court>>(courts));
        }

        [HttpGet("professions")]
        [ProducesResponseType(typeof(Response<IEnumerable<ProfessionDto>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<ProfessionDto>>>> GetProfessions()
        {
            var data = await unitOfWork.Professions.GetAllAsync();
            var mappedData = mapper.Map<IEnumerable<ProfessionDto>>(data);

            return Ok(new Response<IEnumerable<ProfessionDto>>(mappedData));
        }

        [HttpGet("institutions")]
        [ProducesResponseType(typeof(Response<IEnumerable<Institution>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<Institution>>>> GetInstitutions()
        {
            var data = await unitOfWork.Institutions.GetAllAsync();

            return Ok(new Response<IEnumerable<Institution>>(data));
        }

        [HttpGet("social-medias")]
        [ProducesResponseType(typeof(Response<IEnumerable<SocialMedia>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<SocialMedia>>>> GetSocialMedias()
        {
            var data = await unitOfWork.SocialMedias.GetAllAsync();

            return Ok(new Response<IEnumerable<SocialMedia>>(data));
        }

        [HttpPost("district-mediatrs")]
        [ProducesResponseType(typeof(Response<IEnumerable<MediatrItemDto>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<MediatrItemDto>>>> GetDistrictMediatrs([FromBody]MediatrSelectionRequestParameters reqParams)
        {
            var data = await unitOfWork.Mediatrs.GetDistrictMediatrsByProfessionAndCourt(reqParams);
            return Ok(new Response<IEnumerable<DistrictMediatrsDto>>(data));
        }

        [HttpGet("mediatrs/{officeId}")]
        [ProducesResponseType(typeof(Response<IEnumerable<MediatrItemDto>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<MediatrItemDto>>>> GetMediatrsByOffice(int officeId)
        {
            var data = await unitOfWork.Mediatrs.GetAllAsync(x => x.OfficeId == officeId);

            var mappedData = mapper.Map<IEnumerable<MediatrItemDto>>(data);

            return Ok(new Response<IEnumerable<MediatrItemDto>>(mappedData));
        }

        [HttpGet("offices")]
        [ProducesResponseType(typeof(Response<IEnumerable<OfficeReturnDto>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<IEnumerable<OfficeReturnDto>>>> GetOffices()
        {
            var data = await unitOfWork.Offices.GetAllAsync();
            var mappedData = mapper.Map<IEnumerable<OfficeReturnDto>>(data);
            return Ok(new Response<IEnumerable<OfficeReturnDto>>(mappedData));
        }

        [HttpPost("person/by-id-card")]
        public async Task<IActionResult> RevokeToken([FromBody] PersonInfoByIDCard model)
        {
            // accept token from request body or cookie
            var personData = await registryService.GetFullDocByNumber(model.DocSeries, model.DocNumber);

            if(personData == null)
                throw new ApiException("Daxil etdiyiniz Ş/V məlumatları yanlışdır");

            if (personData.Person.Pin.ToLower() != model.PIN.ToLower())
                throw new ApiException("Daxil etdiyiniz FIN kod yanlışdır");

            var mediatrFromDb = await unitOfWork.Mediatrs.GetOneAsync(x => x.PIN.ToLower() == model.PIN.ToLower());
            if (mediatrFromDb != null)
                throw new ApiException("Qeyd etdiyiniz Ş/V məlumatları reyestrdə mövcuddur");

            var imageData = personData.Photo.FileBinary;
            string imagePath = null;
            if (imageData != null)
            {
                string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, @"files\images\mediatrs");
                if (!Directory.Exists(uploadsFolder))
                {
                    Directory.CreateDirectory(uploadsFolder);
                }

                var extension = ImageHelper.GetFileExtensionByBase64(imageData);
                if (extension != null && ImageHelper.IsBase64String(imageData))
                {
                    var guid = Guid.NewGuid();
                    string uniqueFileName = $"{guid.ToString()}.{extension}";
                    string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                    byte[] imageBytes = Convert.FromBase64String(imageData);
                    using (FileStream fs = new FileStream(filePath, FileMode.Create))
                    {
                        fs.Write(imageBytes, 0, imageBytes.Length);
                    }
                    imagePath = $@"files\images\mediatrs\{uniqueFileName}";
                }
            }

            object personObj = new
            {
                FirstName = personData.Person.FirstName,
                LastName = personData.Person.LastName,
                MiddleName = personData.Person.MiddleName,
                DateOfBirth = personData.Person.DateOfBirthStr,
                RegAddress = personData.Address.FullAddress,
                ImagePath = imagePath
            };

            return Ok(new Response<object>(personObj));
        }
    }
}
