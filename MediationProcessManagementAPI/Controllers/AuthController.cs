﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediationProcessManagementAPI.Interfaces;
using MediationProcessManagementAPI.Models.Dtos.Account;

namespace MediationProcessManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private readonly IAccountService accountService;

        public AuthController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        [AllowAnonymous]
        [HttpPost("user/authenticate")]
        public async Task<IActionResult> AuthenticateUserAsync(UserAuthenticationRequest request)
        {
            var response = await accountService.AuthenticateUserAsync(request, generateIPAddress());
            setTokenCookie(response.Data.RefreshToken);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("citizen/authenticate")]
        public async Task<IActionResult> AuthenticateCitizenAsync(CitizenAuthenticationRequest request)
        {
            var response = await accountService.AuthenticateCitizenAsync(request, generateIPAddress());
            setTokenCookie(response.Data.RefreshToken);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("refresh-token")]
        public async Task<IActionResult> RefreshToken()
        {
            var refreshToken = Request.Cookies["refreshToken"];
            var response = await accountService.RefreshToken(refreshToken, generateIPAddress());

            setTokenCookie(response.Data.RefreshToken);

            return Ok(response);
        }

        [HttpPost("revoke-token")]
        public async Task<IActionResult> RevokeToken([FromBody] RevokeTokenRequest model)
        {
            // accept token from request body or cookie
            var token = model.RefreshToken ?? Request.Cookies["refreshToken"];

            var response = await accountService.RevokeToken(token, generateIPAddress());

            return Ok(response);
        }







        #region private methods

        private string generateIPAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

        private void setTokenCookie(string token)
        {
            var cookieOptions = new CookieOptions
            {
                SameSite = SameSiteMode.None,
                Secure = true,
                HttpOnly = true,
                Expires = DateTime.Now.AddDays(7)
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }

        #endregion


    }
}
