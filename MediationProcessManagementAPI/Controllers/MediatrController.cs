﻿using AutoMapper;
using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Helpers;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using MediationProcessManagementAPI.Models.Errors;
using MediationProcessManagementAPI.Models.Parameters;
using MediationProcessManagementAPI.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MediatrController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IWebHostEnvironment hostingEnvironment;

        public MediatrController(IUnitOfWork unitOfWork, IMapper mapper, IWebHostEnvironment hostingEnvironment)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
        }

        [AllowAnonymous]
        [HttpPost("new")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddMediatr(MediatrCreateDto model)
        {
            try
            {
                var mediatrFromDb = await unitOfWork.Mediatrs.GetOneAsync(x => x.PIN.ToLower() == model.PIN.ToLower());
                if (mediatrFromDb != null)
                    throw new ApiException("Qeyd etdiyiniz Ş/V məlumatları reyestrdə mövcuddur");

                if (model.Certificates.Any())
                {
                    string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, @"files\images\certificates");
                    if (!Directory.Exists(uploadsFolder))
                    {
                        Directory.CreateDirectory(uploadsFolder);
                    }

                    foreach (var certificate in model.Certificates)
                    {
                        if(certificate.ImageData != null)
                        {
                            var extension = ImageHelper.GetFileExtensionByBase64(certificate.ImageData);
                            if (extension != null && ImageHelper.IsBase64String(certificate.ImageData))
                            {
                                var guid = Guid.NewGuid();
                                string uniqueFileName = $"{guid.ToString()}.{extension}";
                                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                                byte[] imageBytes = Convert.FromBase64String(certificate.ImageData);
                                using (FileStream fs = new FileStream(filePath, FileMode.Create))
                                {
                                    fs.Write(imageBytes, 0, imageBytes.Length);
                                }
                                certificate.FilePath = $@"files\images\certificates\{uniqueFileName}";
                            }
                            else
                                throw new ApiException("Sertifikat faylı düzgün əlavə edilməyib");
                        }
                    }
                }
                var mappedModel = mapper.Map<Mediatr>(model);

                var createdMediatr = await unitOfWork.Mediatrs.AddAsync(mappedModel);
            }
            catch (Exception ex)
            {
                throw new ApiException($"Mediator məlumatlarını əlavə edərkən xəta baş verdi. Bir az sonra yenidən cəhd edin.");
            }

            return Ok(new Response<object>(null, "Mediator məlumatları reyestrə uğurla əlavə edildi"));
        }

        [Authorize(Roles = "Council")]
        [HttpGet("all")]
        [ProducesResponseType(typeof(Response<PagedResponse<MediatrTableDataDto>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<PagedResponse<MediatrTableDataDto>>>> GetAllMediatrs([FromQuery] MediatrRequestParameters reqParams)
        {
            var mediatrs = await unitOfWork.Mediatrs.GetPagedMediatrs(reqParams);

            return Ok(new Response<PagedResponse<MediatrTableDataDto>>(mediatrs));
        }

        [HttpGet("{mediatrId}")]
        [ProducesResponseType(typeof(Response<MediatrExtendedItemDto>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Response<MediatrExtendedItemDto>>> GetMediatrById(int mediatrId)
        {
            var mediatrFromDb = await unitOfWork.Mediatrs.GetExtendedMediatrById(mediatrId);

            var mappedMediatr = mapper.Map<MediatrExtendedItemDto>(mediatrFromDb);

            return Ok(new Response<MediatrExtendedItemDto>(mappedMediatr));
        }
    }
}

