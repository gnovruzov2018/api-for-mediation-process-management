﻿using Aspose.Words;
using AutoMapper;
using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Interfaces;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using MediationProcessManagementAPI.Models.Enums;
using MediationProcessManagementAPI.Models.Errors;
using MediationProcessManagementAPI.Models.Parameters;
using MediationProcessManagementAPI.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RequestController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IAuthenticatedUserService authenticatedUser;
        private readonly IWebHostEnvironment hostingEnvironment;

        private readonly int userId;
        private readonly string roleName;
        private readonly string mediatrId;
        private readonly string personId;
        private readonly string officeId;

        public RequestController(IUnitOfWork unitOfWork,
                                 IAuthenticatedUserService authenticatedUser,
                                 IWebHostEnvironment hostingEnvironment,
                                 IMapper mapper)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.authenticatedUser = authenticatedUser ?? throw new ArgumentNullException(nameof(authenticatedUser));
            this.hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));

            this.userId = this.authenticatedUser.UserId;
            this.roleName = this.authenticatedUser.RoleName;
            this.mediatrId = this.authenticatedUser.MediatrId;
            this.personId = this.authenticatedUser.PersonId;
            this.officeId = this.authenticatedUser.OfficeId;
        }

        [HttpPost("new")]
        [ProducesResponseType(typeof(Response<RequestReturnDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<RequestReturnDto>>> AddRequest([FromBody] RequestCreateDto model)
        {
            List<RequestedDistrict> requestedDistricts = new List<RequestedDistrict>();
            List<RequestedMediatr> requestedMediatrs = new List<RequestedMediatr>();
            RequestReturnDto requestToReturn = new RequestReturnDto();

            try
            {
                foreach (int districtId in model.DistrictIds)
                    requestedDistricts.Add(new RequestedDistrict() { DistrictId = districtId });

                foreach (int mediatrId in model.MediatrIds)
                    requestedMediatrs.Add(new RequestedMediatr() { MediatrId = mediatrId, MediatrStatus = MediatrStatus.Pending });

                var request = new Request()
                {
                    ProfessionId = model.ProfessionId,
                    CourtId = model.CourtId,
                    ConflictInfo = model.ConflictInfo,
                    CourtCaseInfo = model.CourtCaseInfo,
                    PrefferedSessionTime = model.PrefferedSessionTime,
                    RequiredLangs = model.RequiredLangs,
                    CaseInAction = model.CaseInAction,
                    StatusId = 1,
                    OfficeId = model.OfficeId,
                    MediatrSelected = model.OfficeId == null,
                    UserId = userId,
                    Sides = mapper.Map<List<Side>>(model.Sides.ToList()),
                    RequestedDistricts = requestedDistricts,
                    RequestedMediatrs = requestedMediatrs
                };

                var createdRequest = await unitOfWork.Requests.AddAsync(request);
                var newRequest = await unitOfWork.Requests.GetRequestById(createdRequest.Id);
                requestToReturn = mapper.Map<RequestReturnDto>(newRequest);

            }
            catch (Exception ex)
            {
                throw new ApiException($"Müraciəti qeydə alarkən xəta baş verdi");
            }

            return Ok(new Response<RequestReturnDto>(requestToReturn, "Hörmətli istifadəçi, müraciətiniz uğurla qeydə alındı."));
        }

        [AllowAnonymous]
        [HttpGet("{requestId}")]
        [ProducesResponseType(typeof(Response<RequestFullReturnDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<RequestFullReturnDto>>> GetById(int requestId)
        {
            try
            {
                var request = await unitOfWork.Requests.GetRequestById(requestId);

                if (request == null)
                    throw new ApiException("Müraciəti aşkar edilmədi");

                var mappedRequest = mapper.Map<RequestFullReturnDto>(request);
                return Ok(new Response<RequestFullReturnDto>(mappedRequest));
            }
            catch (Exception ex)
            {
                throw new ApiException("Xəta baş verdi");
            }

        }

        [Authorize(Roles = "Admin,Council")]
        [HttpGet("councils/all")]
        [ProducesResponseType(typeof(Response<PagedResponse<RequestReturnDto>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<PagedResponse<RequestReturnDto>>>> GetRequestsForAdmin([FromQuery] RequestParameters reqParams)
        {
            var requests = await unitOfWork.Requests.GetPagedRequestsForAdmin(reqParams);
            return Ok(new Response<PagedResponse<RequestReturnDto>>(requests));
        }

        [Authorize(Roles = "Office")]
        [HttpGet("offices/all")]
        [ProducesResponseType(typeof(Response<PagedResponse<RequestReturnDto>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<PagedResponse<RequestReturnDto>>>> GetRequestsForOffices([FromQuery] RequestParameters reqParams)
        {
            var requests = await unitOfWork.Requests.GetPagedRequestsForOffices(int.Parse(officeId), reqParams);
            return Ok(new Response<PagedResponse<RequestReturnDto>>(requests));
        }

        [Authorize(Roles = "Mediatr")]
        [HttpGet("mediatrs/all")]
        [ProducesResponseType(typeof(Response<PagedResponse<RequestReturnDto>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<PagedResponse<RequestReturnDto>>>> GetRequestsForMediatrs([FromQuery] RequestParameters reqParams)
        {
            var requests = await unitOfWork.Requests.GetPagedRequestsForMediatrs(int.Parse(mediatrId), reqParams);
            return Ok(new Response<PagedResponse<RequestReturnDto>>(requests));
        }

        [Authorize(Roles = "Citizen")]
        [HttpGet("citizens/all")]
        [ProducesResponseType(typeof(Response<PagedResponse<RequestReturnDto>>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<PagedResponse<RequestReturnDto>>>> GetRequests([FromQuery] RequestParameters reqParams)
        {
            var requests = await unitOfWork.Requests.GetPagedRequestsForUsers(userId, reqParams);
            return Ok(new Response<PagedResponse<RequestReturnDto>>(requests));
        }

        [Authorize]
        [HttpGet("print/{requestId}")]
        [ProducesResponseType(typeof(Response<string>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<string>>> Print(int requestId)
        {
            try
            {
                var request = await unitOfWork.Requests.GetRequestById(requestId);

                if(request == null)
                    throw new ApiException("Müraciət aşkar edilmədi");

                if(request.Mediatr == null)
                    throw new ApiException("Sənədin çap versiyasını nəzərdən keçirmək, mediator təyin edildikdən sonra mümkündür");

                var person = request.User.Person;

                var sides = request.Sides
                    .Select(x => $"{x.SideLastName} {x.SideFirstName} {x.SideMiddleName} {(x.SideGender == Gender.Male ? "oğlu" : "qızı")}").ToArray();
                var firstSide = $"{person.LastName} {person.FirstName} {person.MiddleName} ({person.RegAddress}, {person.Email})";
                var mediatrName = $"{request.Mediatr.LastName} {request.Mediatr.FirstName} {request.Mediatr.MiddleName}";
                string templatePath = Path.Combine(hostingEnvironment.WebRootPath, @"files\word-templates\proposal.docx");
                Document document = new Document(templatePath);
                string[] docObjectNames = { "CreateDate", "ApplicantFullName", "SidesFullName", "ConflictInfo", "MediatrFullName", "ApplicantName" };
                object[] docObjects = {
                    DateTime.Now.ToString("dd/MM/yyyy"),
                    firstSide,
                    string.Join(", ", sides),
                    request.ConflictInfo,
                    mediatrName,
                    $"{person.LastName} {person.FirstName} {person.MiddleName}"
                };
                document.MailMerge.Execute(docObjectNames, docObjects);

                string saveFolder = $@"files\pdfs\{DateTime.Now.Month}\{DateTime.Now.Year}";
                string documentSavePath = Path.Combine(hostingEnvironment.WebRootPath, $@"pdfs\{DateTime.Now.Month}\{DateTime.Now.Year}");
                if (!Directory.Exists(documentSavePath))
                    Directory.CreateDirectory(documentSavePath);

                string fileName = $"{DateTime.Now.ToString("yyyyMMddTHHmmss")}.pdf";
                string filePath = Path.Combine(documentSavePath, fileName);

                document.Save(filePath);
                return Ok(new Response<string>(Path.Combine(saveFolder, fileName), ""));
            }
            catch (Exception ex)
            {
                throw new ApiException("Müraciəti çap edərkən xəta baş verdi");
            }

        }

        [Authorize(Roles = "Mediatr")]
        [HttpGet("approve/{requestId}")]
        [ProducesResponseType(typeof(Response<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<RequestReturnDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<RequestReturnDto>>> ApproveRequest(int requestId)
        {
            try
            {
                var request = await unitOfWork.Requests.GetRequestById(requestId);

                if (request == null)
                    throw new ApiException("Müraciəti aşkar edilmədi");

                int mediatrIdInt = int.Parse(mediatrId);

                if (request.StatusId == 2)
                    return BadRequest(new Response<string>("Müraciət artıq digər mediator tərəfindən qəbul edilib"));
                if (request.StatusId == 4)
                    return BadRequest(new Response<string>("Müraciətin vaxtı bitib"));

                request.MediatrId = mediatrIdInt;
                request.AcceptDate = DateTime.Now;
                request.StatusId = 2;
                request.RequestedMediatrs.FirstOrDefault(x => x.MediatrId == mediatrIdInt).MediatrStatus = MediatrStatus.Accepted;

                await unitOfWork.Requests.UpdateAsync(request);

                var updatedRequest = await unitOfWork.Requests.GetRequestById(requestId);

                var mappedRequest = mapper.Map<RequestReturnDto>(updatedRequest);
                return Ok(new Response<RequestReturnDto>(mappedRequest, "Müraciət uğurla qəbul edildi"));
            }
            catch (Exception ex)
            {
                throw new ApiException("Xəta baş verdi");
            }
        }

        [Authorize(Roles = "Mediatr")]
        [HttpPost("reject/{requestId}")]
        [ProducesResponseType(typeof(Response<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<RequestReturnDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<RequestReturnDto>>> RejectRequest([FromRoute]int requestId, [FromBody] RequestRejectDto rejectDto)
        {
            try
            {
                var request = await unitOfWork.Requests.GetRequestById(requestId);

                if (request == null)
                    throw new ApiException("Müraciəti aşkar edilmədi");

                int mediatrIdInt = int.Parse(mediatrId);

                if (request.StatusId == 4)
                    return BadRequest(new Response<string>("Müraciətin vaxtı bitib"));

                if(request.RequestedMediatrs.FirstOrDefault(x => x.MediatrId == mediatrIdInt).MediatrStatus == MediatrStatus.Rejected)
                    return BadRequest(new Response<string>("Siz daha öncə müraciətdən imtina etmisiniz"));

                request.RequestedMediatrs.FirstOrDefault(x => x.MediatrId == mediatrIdInt).MediatrStatus = MediatrStatus.Rejected;
                request.RequestedMediatrs.FirstOrDefault(x => x.MediatrId == mediatrIdInt).DeclineDate = DateTime.Now;
                request.RequestedMediatrs.FirstOrDefault(x => x.MediatrId == mediatrIdInt).RejectText = rejectDto.RejectText;

                if (request.RequestedMediatrs.All(x => x.MediatrStatus == MediatrStatus.Rejected))
                    request.StatusId = 3;

                await unitOfWork.Requests.UpdateAsync(request);

                var updatedRequest = await unitOfWork.Requests.GetRequestById(requestId);

                var mappedRequest = mapper.Map<RequestReturnDto>(updatedRequest);
                return Ok(new Response<RequestReturnDto>(mappedRequest, "Müraciətdən imtina edildi"));
            }
            catch (Exception ex)
            {
                throw new ApiException("Xəta baş verdi");
            }
        }

        [Authorize(Roles = "Office")]
        [HttpGet("assign-mediatr/{requestId}/{mediatrId}")]
        [ProducesResponseType(typeof(Response<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<RequestReturnDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<RequestReturnDto>>> AssignRequest(int requestId, int mediatrId)
        {
            try
            {
                var request = await unitOfWork.Requests.GetRequestById(requestId);

                if (request == null)
                    throw new ApiException("Müraciəti aşkar edilmədi");

                request.MediatrId = mediatrId;
                request.AcceptDate = DateTime.Now;
                request.StatusId = 2;

                await unitOfWork.Requests.UpdateAsync(request);

                var updatedRequest = await unitOfWork.Requests.GetRequestById(requestId);

                var mappedRequest = mapper.Map<RequestReturnDto>(updatedRequest);
                return Ok(new Response<RequestReturnDto>(mappedRequest, "Müraciət seçilmiş mediatora təyin edildi"));
            }
            catch (Exception ex)
            {
                throw new ApiException("Xəta baş verdi");
            }
        }

        [Authorize(Roles = "Council")]
        [HttpGet("assign-mediatr/{requestId}")]
        [ProducesResponseType(typeof(Response<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<RequestReturnDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Response<RequestReturnDto>>> AssignRequest(int requestId)
        {
            try
            {
                var request = await unitOfWork.Requests.GetRequestById(requestId);

                if (request == null)
                    throw new ApiException("Müraciəti aşkar edilmədi");

                if(request.StatusId != 2)
                    throw new ApiException("Yalnız qəbul edilmiş müraciətə mediator təyin oluna bilər");

                var districtIds = string.Join(',', request.RequestedDistricts.Select(x => x.DistrictId));
                int mediatrId = await unitOfWork.Mediatrs.GetMediatrIdByWorkload(districtIds);

                if(mediatrId == 0)
                    throw new ApiException("Sistem tərəfindən seçilmiş əraziyə uyğun mediator tapılmadı");

                request.MediatrId = mediatrId;
                request.AcceptDate = DateTime.Now;
                request.StatusId = 2;

                await unitOfWork.Requests.UpdateAsync(request);

                var updatedRequest = await unitOfWork.Requests.GetRequestById(requestId);

                var mappedRequest = mapper.Map<RequestReturnDto>(updatedRequest);
                return Ok(new Response<RequestReturnDto>(mappedRequest, "Sistem tərəfindən müraciətə mediator təyin edildi"));
            }
            catch (Exception ex)
            {
                throw new ApiException("Xəta baş verdi");
            }
        }

    }
}
