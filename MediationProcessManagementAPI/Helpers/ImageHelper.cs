﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Helpers
{
    public static class ImageHelper
    {
        public static string GetFileExtensionByBase64(string data)
        {
            var dataSubStr = data.Substring(0, 5).ToUpper();
            switch (dataSubStr)
            {
                case "IVBOR":
                    return "png";
                case "/9J/4":
                    return "jpg";
                case "JVBER":
                    return "pdf";
                default:
                    return null;
            }
        }

        public static bool IsBase64String(string base64)
        {
            base64 = base64.Trim();
            return (base64.Length % 4 == 0) && Regex.IsMatch(base64, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }
    }
}
