﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediationProcessManagementAPI.Wrappers
{
    public class PagedResponse<T>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public bool HasNextPage
        {
            get
            {
                return (PageNumber < TotalPages);
            }
        }
        public bool HasPreviousPage
        {
            get
            {
                return (PageNumber > 1);
            }
        }
        public int TotalPages { 
            get
            {
                return (int)Math.Ceiling(TotalCount / (double)PageSize);
            } 
        }

        public IList<T> Data { get; set; }

        public PagedResponse()
        {

        }

        public PagedResponse(IList<T> data, int count, int pageNumber, int pageSize)
        {
            TotalCount = count;
            Data = data;
            PageNumber = pageNumber;
            PageSize = pageSize;
        }
    }
}
