using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediationProcessManagementAPI.Data.Contracts;
using MediationProcessManagementAPI.Data.Seeds;
using MediationProcessManagementAPI.Interfaces;
using MediationProcessManagementAPI.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace MediationProcessManagementAPI
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var unitOfWork = services.GetRequiredService<IUnitOfWork>();

                    await RolesSeed.SeedAsync(unitOfWork);
                    await StatusesSeed.SeedAsync(unitOfWork);
                    await DistrictsSeed.SeedAsync(unitOfWork);
                    await CourtsSeed.SeedAsync(unitOfWork);
                    await ProfessionsSeed.SeedAsync(unitOfWork);
                    await InstitutionsSeed.SeedAsync(unitOfWork);
                    await SocialMediasSeed.SeedAsync(unitOfWork);
                    //await MediatrsSeed.SeedAsync(unitOfWork);
                    await OfficesSeed.SeedAsync(unitOfWork);
                    await UsersSeed.SeedAsync(unitOfWork);
                    await DistrictCourtsSeed.SeedAsync(unitOfWork);
                    //await MediatrProfessionsSeed.SeedAsync(unitOfWork);
                    //await DistrictMediatrsSeed.SeedAsync(unitOfWork);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
