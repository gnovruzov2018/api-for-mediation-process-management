﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Mediatr : EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string DateOfBirth { get; set; }
        public string ImagePath { get; set; }
        public string PIN { get; set; }
        public string DocSeries { get; set; }
        public string DocNumber { get; set; }
        public string RegAddress { get; set; }
        public string ActingAddress { get; set; }
        public DateTime MembershipDate { get; set; }
        public string LanguageSkills { get; set; }
        public string VOEN { get; set; }
        public string RegistryNumber { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PersonalPageLink { get; set; }
        public string OtherWorkplace { get; set; }
        public string OtherPosition { get; set; }

        public int? OfficeId { get; set; }
        public Office Office { get; set; }
        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }

        public virtual ICollection<MediatrProfession> MediatrProfessions { get; set; }
        public virtual ICollection<DistrictMediatr> DistrictMediatrs { get; set; }
        public virtual ICollection<MediatrSocialMedia> MediatrsSocialMedias { get; set; }
        public virtual ICollection<Education> Educations { get; set; }
        public virtual ICollection<Certificate> Certificates { get; set; }
    }
}
