﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class User : EntityBase
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public int? OfficeId { get; set; }
        public int? MediatrId { get; set; }
        public int? PersonId { get; set; }
        public int RoleId { get; set; }

        public Office Office { get; set; }
        public Mediatr Mediatr { get; set; }
        public Person Person { get; set; }
        public Role Role { get; set; }

        public ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
