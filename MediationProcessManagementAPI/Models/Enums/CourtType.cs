﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Enums
{
    public enum CourtType
    {
        District = 1,
        Appeal = 2,
        Commercial = 3
    }
}
