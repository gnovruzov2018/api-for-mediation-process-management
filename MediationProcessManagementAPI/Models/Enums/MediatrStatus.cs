﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Enums
{
    public enum MediatrStatus
    {
        Pending,
        Accepted,
        Rejected
    }
}
