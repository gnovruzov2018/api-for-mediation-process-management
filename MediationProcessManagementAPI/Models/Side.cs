﻿using MediationProcessManagementAPI.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Side : EntityBase
    {
        public string SideFirstName { get; set; }
        public string SideLastName { get; set; }
        public string SideMiddleName { get; set; }
        public Gender SideGender { get; set; }
        public string AdvocateFirstName { get; set; }
        public string AdvocateLastName { get; set; }
        public string AdvocateMiddleName { get; set; }
        public string OrganizationName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public int RequestId { get; set; }
        public Request Request { get; set; }
    }
}
