﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Validators
{
    public class MediatorValidator : AbstractValidator<Mediatr>
    {
        public MediatorValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().NotNull().WithMessage("'Ad' boş qoyula bilməz")
                .Length(2, 25).WithMessage("'Ad' minimum 2 maksimum 25 simvoldan ibarət ola bilər");
            RuleFor(x => x.LastName).NotEmpty().NotNull().WithMessage("'Soyad' boş qoyula bilməz")
                .Length(2, 25).WithMessage("'Soyad' minimum 2 maksimum 25 simvoldan ibarət ola bilər");
            RuleFor(x => x.MiddleName).NotEmpty().NotNull().WithMessage("'Ata adı' boş qoyula bilməz")
                .Length(2, 25).WithMessage("'Ata adı' minimum 2 maksimum 25 simvoldan ibarət ola bilər");
            RuleFor(x => x.DateOfBirth).NotEmpty().NotNull().WithMessage("'Doğum tarixi' boş qoyula bilməz");
            RuleFor(x => x.PIN).NotEmpty().NotNull().WithMessage("'FİN' boş qoyula bilməz")
                .Length(5, 7).WithMessage("'FİN' minimum 5 maksimum 7 simvoldan ibarət ola bilər");
            RuleFor(x => x.DocSeries).NotEmpty().NotNull().WithMessage("'Ş/V seriyası' boş qoyula bilməz");
            RuleFor(x => x.DocNumber).NotEmpty().NotNull().WithMessage("'Ş/V nömrəsi' boş qoyula bilməz");
            RuleFor(x => x.RegAddress).NotEmpty().NotNull().WithMessage("'Qeydiyyat ünvanı' boş qoyula bilməz");
            RuleFor(x => x.ActingAddress).NotEmpty().NotNull().WithMessage("'Fəaliyyət göstərdiyi ünvan' boş qoyula bilməz");
            RuleFor(x => x.MembershipDate).NotEmpty().Must(BeAValidDate).WithMessage("'Mediasiya Şurasının üzvlüyünə qəbul edildiyi tarix' boş qoyula bilməz");
            RuleFor(x => x.Email).EmailAddress().WithMessage("'Elektron poçt ünvanı' düzgün daxil edilməyib")
                .NotEmpty().NotNull().WithMessage("'Elektron poçt ünvanı' boş qoyula bilməz");
            RuleFor(x => x.Phone).Length(10).WithMessage("'Telefon nömrəsi' düzgün daxil edilməyib")
                .NotEmpty().NotNull().WithMessage("'Telefon nömrəsi' boş qoyula bilməz");
            RuleFor(x => x.RegistryNumber).NotEmpty().NotNull().WithMessage("'Reyestr nömrəsi' boş qoyula bilməz");
            RuleFor(x => x.VOEN).NotEmpty().NotNull().WithMessage("'VÖEN' boş qoyula bilməz");
            RuleFor(x => x.LanguageSkills).NotEmpty().NotNull().WithMessage("'Dil bilikləri' boş qoyula bilməz");
            RuleFor(x => x.ActingAddress).NotEmpty().NotNull().WithMessage("'Fəaliyyət göstərdiyi ünvan' boş qoyula bilməz");
            RuleFor(x => x.InstitutionId).NotEmpty().WithMessage("'Təlim qurumu' boş qoyula bilməz");
            RuleFor(x => x.Educations).NotEmpty().NotNull().WithMessage("'Təhsil barədə məlumatlar' boş qoyula bilməz");
            RuleFor(x => x.MediatrProfessions).NotEmpty().NotNull().WithMessage("'İxtisas kateqoriyası' boş qoyula bilməz");
        }

        private bool BeAValidDate(DateTime date)
        {
            if (date == default(DateTime))
                return false;
            return true;
        }

        
    }
}
