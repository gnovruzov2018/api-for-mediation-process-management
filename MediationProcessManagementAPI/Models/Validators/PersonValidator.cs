﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Validators
{
    public class PersonValidator : AbstractValidator<Person>
    {
        public PersonValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().NotNull().WithMessage("'Ad' boş qoyula bilməz")
                .Length(2, 25).WithMessage("'Ad' minimum 2 maksimum 25 simvoldan ibarət ola bilər");
            RuleFor(x => x.LastName).NotEmpty().NotNull().WithMessage("'Soyad' boş qoyula bilməz")
                .Length(2, 25).WithMessage("'Soyad' minimum 2 maksimum 25 simvoldan ibarət ola bilər");
            RuleFor(x => x.MiddleName).NotEmpty().NotNull().WithMessage("'Ata adı' boş qoyula bilməz")
                .Length(2, 25).WithMessage("'Ata adı' minimum 2 maksimum 25 simvoldan ibarət ola bilər");
            RuleFor(x => x.DateOfBirth).NotEmpty().NotNull().WithMessage("'Doğum tarixi' boş qoyula bilməz");
            RuleFor(x => x.PIN).NotEmpty().NotNull().WithMessage("'FİN' boş qoyula bilməz")
                .Length(5, 7).WithMessage("'FİN' minimum 5 maksimum 7 simvoldan ibarət ola bilər");
            RuleFor(x => x.DocSeries).NotEmpty().NotNull().WithMessage("'Ş/V seriyası' boş qoyula bilməz");
            RuleFor(x => x.DocNumber).NotEmpty().NotNull().WithMessage("'Ş/V nömrəsi' boş qoyula bilməz");
        }
    }
}
