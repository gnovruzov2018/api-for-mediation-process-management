﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Parameters
{
    public class PersonInfoByIDCard
    {
        public string DocSeries { get; set; }
        public string DocNumber { get; set; }
        public string PIN { get; set; }
    }
}
