﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Parameters
{
    public class PagingParameters
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public PagingParameters()
        {
            this.PageNumber = 1;
            this.PageSize = 10;
        }
        public PagingParameters(int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 10 ? 10 : pageSize;
        }
    }

    public class RequestParameters : PagingParameters
    {

    }

    public class MediatrRequestParameters : RequestParameters
    {

    }

    public class MediatrSelectionRequestParameters
    {
        public int ProfessionId { get; set; }
        public int? CourtId { get; set; }
        public int[] DistrictIds { set; get; }
    }
}
