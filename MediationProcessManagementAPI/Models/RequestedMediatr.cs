﻿using MediationProcessManagementAPI.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class RequestedMediatr
    {
        public int RequestId { get; set; }
        public Request Request { get; set; }

        public int MediatrId { get; set; }
        public Mediatr Mediatr { get; set; }

        public MediatrStatus MediatrStatus { get; set; }
        public DateTime? DeclineDate { get; set; }
        public string RejectText { get; set; }
    }
}
