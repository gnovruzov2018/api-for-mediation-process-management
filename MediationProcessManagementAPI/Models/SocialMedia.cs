﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class SocialMedia
    {
        public int Id { get; set; }
        public string SocialMediaName { get; set; }
    }
}
