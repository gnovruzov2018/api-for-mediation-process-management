﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class District
    {
        public int Id { get; set; }
        public string DistrictName { get; set; }
        public string DistrictCode { get; set; }
        public int? ParentId { get; set; }

        [JsonIgnore]
        [ForeignKey("ParentId")]
        public District ParentDistrict { get; set; }

        public virtual ICollection<District> ChildDistricts { get; set; }

        [JsonIgnore]
        public ICollection<DistrictCourt> DistrictCourts { get; set; }
    }
}
