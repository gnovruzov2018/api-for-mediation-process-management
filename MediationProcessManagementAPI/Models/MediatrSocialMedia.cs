﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class MediatrSocialMedia
    {
        public int SocialMediaId { get; set; }
        public int MediatrId { get; set; }

        public SocialMedia SocialMedia { get; set; }
        public Mediatr Mediatr { get; set; }

        public string LinkToPage { get; set; }
    }
}
