﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Education : EntityBase
    {
        public string CollegeName { get; set; }
        public string MajorName { get; set; }
        public DateTime GraduationDate { get; set; }
        public int MediatrId { get; set; }

        public Mediatr Mediatr { get; set; }
    }
}
