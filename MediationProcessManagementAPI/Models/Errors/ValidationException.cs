﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Errors
{
    public class ValidationException : Exception
    {
        public ValidationException() : base("One or more validation failures have occurred.")
        {
            Errors = new List<string>();
        }
        public List<string> Errors { get; }
        public ValidationException(List<string> failures)
            : this()
        {
            foreach (var failure in failures)
            {
                Errors.Add(failure);
            }
        }

    }
}
