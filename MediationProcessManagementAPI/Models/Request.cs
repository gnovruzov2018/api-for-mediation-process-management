﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Request : EntityBase
    {
        public string ConflictInfo { get; set; }
        public string CourtCaseInfo { get; set; }
        public string PrefferedSessionTime { get; set; }
        public string RequiredLangs { get; set; }
        public bool CaseInAction { get; set; }
        public bool MediatrSelected { get; set; }
        public DateTime? AcceptDate { get; set; }

        public int? OfficeId { get; set; }
        public Office Office { get; set; }

        public int? CourtId { get; set; }
        public Court Court { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int? MediatrId { get; set; }
        public Mediatr Mediatr { get; set; }

        public int StatusId { get; set; }
        public Status Status { get; set; }

        public int ProfessionId { get; set; }
        public Profession Profession { get; set; }

        public virtual ICollection<Side> Sides { get; set; }
        public virtual ICollection<RequestedMediatr> RequestedMediatrs { get; set; }
        public virtual ICollection<RequestedDistrict> RequestedDistricts { get; set; }
    }
}
