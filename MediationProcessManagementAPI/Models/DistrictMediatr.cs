﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class DistrictMediatr
    {
        public int DistrictId { get; set; }
        public District District { get; set; }

        public int MediatrId { get; set; }
        public Mediatr Mediatr { get; set; }
    }
}
