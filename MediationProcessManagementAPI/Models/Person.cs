﻿using MediationProcessManagementAPI.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Person : EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string DateOfBirth { get; set; }
        public string PIN { get; set; }
        public string DocSeries { get; set; }
        public string DocNumber { get; set; }
        public string RegAddress { get; set; }
        public string CurrentAddress { get; set; }
        public string Image { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public virtual User User { get; set; }

    }
}
