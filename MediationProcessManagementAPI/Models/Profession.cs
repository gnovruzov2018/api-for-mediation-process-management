﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Profession
    {
        public int Id { get; set; }
        public string ProfessionName { get; set; }
        public string ProfessionDesc { get; set; }

        public virtual ICollection<MediatrProfession> MediatrProfessions { get; set; }
    }
}
