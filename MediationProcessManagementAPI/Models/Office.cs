﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Office : EntityBase
    {
        public string OfficeName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public virtual ICollection<Mediatr> Mediatrs { get; set; }
    }
}
