﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class MediatrProfession
    {
        public int MediatrId { get; set; }
        public Mediatr Mediatr { get; set; }

        public int ProfessionId { get; set; }
        public Profession Profession { get; set; }
    }
}
