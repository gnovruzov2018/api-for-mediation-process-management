﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class RequestedDistrict
    {
        public int RequestId { get; set; }
        public Request Request { get; set; }

        public int DistrictId { get; set; }
        public District District { get; set; }
    }
}
