﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class DistrictCourt
    {
        public int DistrictId { get; set; }
        public District District { get; set; }

        public int CourtId { get; set; }
        public Court Court { get; set; }
    }
}
