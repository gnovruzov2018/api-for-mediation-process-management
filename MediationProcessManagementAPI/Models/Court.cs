﻿using MediationProcessManagementAPI.Models.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Court
    {
        public int Id { get; set; }
        public string CourtName { get; set; }
        public CourtType CourtType { get; set; }

        [JsonIgnore]
        public virtual ICollection<DistrictCourt> DistrictCourts { get; set; }
    }
}
