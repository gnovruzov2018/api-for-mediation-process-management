﻿namespace MediationProcessManagementAPI.Models.Dtos.Account
{
    public class UserAuthenticationRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
