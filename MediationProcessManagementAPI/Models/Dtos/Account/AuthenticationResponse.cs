﻿using System.Text.Json.Serialization;

namespace MediationProcessManagementAPI.Models.Dtos.Account
{
    public class AuthenticationResponse
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string JWToken { get; set; }
        public Person Person { get; set; }
        public Office Office { get; set; }
        public Mediatr Mediatr { get; set; }

        [JsonIgnore]
        public string RefreshToken { get; set; }
    }
}
