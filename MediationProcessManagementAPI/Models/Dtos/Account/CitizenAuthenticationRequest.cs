﻿namespace MediationProcessManagementAPI.Models.Dtos.Account
{
    public class CitizenAuthenticationRequest
    {
        public string DocSeries { get; set; }
        public string DocNumber { get; set; }
        public string PIN { get; set; }
        public string DateOfBirth { get; set; }
    }
}
