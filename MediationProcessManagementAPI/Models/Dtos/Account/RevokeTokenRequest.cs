﻿namespace MediationProcessManagementAPI.Models.Dtos.Account
{
    public class RevokeTokenRequest
    {
        public string RefreshToken { get; set; }
    }
}
