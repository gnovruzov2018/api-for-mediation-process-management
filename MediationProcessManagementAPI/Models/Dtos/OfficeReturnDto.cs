﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Dtos
{
    public class OfficeReturnDto
    {
        public int Id { get; set; }
        public string OfficeName { get; set; }
    }
}
