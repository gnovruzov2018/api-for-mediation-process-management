﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Dtos
{
    public class MediatrItemDto
    {
        public int MediatrId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string RegistryNumber { get; set; }
        public string ImagePath { get; set; }

        public virtual ICollection<ProfessionDto> MediatrProfessions { get; set; }
    }

    public class MediatrCreateDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string DateOfBirth { get; set; }
        public string PIN { get; set; }
        public string DocSeries { get; set; }
        public string DocNumber { get; set; }
        public string RegAddress { get; set; }
        public string ActingAddress { get; set; }
        public string ImagePath { get; set; }
        public DateTime MembershipDate { get; set; }
        public string LanguageSkills { get; set; }
        public string VOEN { get; set; }
        public string RegistryNumber { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PersonalPageLink { get; set; }
        public string OtherWorkplace { get; set; }
        public string OtherPosition { get; set; }

        public int? OfficeId { get; set; }
        public int InstitutionId { get; set; }

        public int[] MediatrProfessions { get; set; }
        public int[] DistrictMediatrs { get; set; }
        public MediatrSocialMediaCreateDto[] MediatrsSocialMedias { get; set; }
        public EducationCreateDto[] Educations { get; set; }
        public CertificateCreateDto[] Certificates { get; set; }
    }

    public class MediatrSocialMediaCreateDto
    {
        public int SocialMediaId { get; set; }
        public string LinkToPage { get; set; }
    }

    public class EducationCreateDto
    {
        public string CollegeName { get; set; }
        public string MajorName { get; set; }
        public DateTime GraduationDate { get; set; }
    }

    public class EducationDto
    {
        public string CollegeName { get; set; }
        public string MajorName { get; set; }
        public DateTime GraduationDate { get; set; }
    }

    public class CertificateCreateDto
    {
        public string IssuerInstitution { get; set; }
        public string CertificateName { get; set; }
        public DateTime CertificationDate { get; set; }
        public string ImageData { get; set; }

        [JsonIgnore]
        public string FilePath { get; set; }
    }

    public class CertificateDto
    {
        public string IssuerInstitution { get; set; }
        public string CertificateName { get; set; }
        public DateTime CertificationDate { get; set; }
        public string FilePath { get; set; }
    }

    public class MediatrSocialMediaDto
    {
        public string SocialMediaName { get; set; }
        public string LinkToPage { get; set; }
    }

    public class MediatrTableDataDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PIN { get; set; }
        public string RegistryNumber { get; set; }
        public string Districts { get; set; }
    }

    public class MediatrExtendedItemDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string DateOfBirth { get; set; }
        public string PIN { get; set; }
        public string DocSeries { get; set; }
        public string DocNumber { get; set; }
        public string RegAddress { get; set; }
        public string ActingAddress { get; set; }
        public string ImagePath { get; set; }
        public DateTime MembershipDate { get; set; }
        public string LanguageSkills { get; set; }
        public string VOEN { get; set; }
        public string RegistryNumber { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PersonalPageLink { get; set; }
        public string OtherWorkplace { get; set; }
        public string OtherPosition { get; set; }

        public string OfficeName { get; set; }
        public string InstitutionName { get; set; }

        public List<ProfessionDto> MediatrProfessions { get; set; }
        public List<string> Districts { get; set; }
        public List<MediatrSocialMediaDto> MediatrsSocialMedias { get; set; }
        public List<EducationDto> Educations { get; set; }
        public List<CertificateDto> Certificates { get; set; }
    }
}
