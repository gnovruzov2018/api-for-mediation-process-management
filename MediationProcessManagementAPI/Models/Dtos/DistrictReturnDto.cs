﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Dtos
{
    public class DistrictReturnDto
    {
        public int Id { get; set; }
        public string DistrictName { get; set; }
    }

    public class DistrictMediatrsDto
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public IList<MediatrItemDto> Mediatrs { get; set; }
    }
}
