﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Dtos
{
    public class ProfessionDto
    {
        public int Id { get; set; }
        public string ProfessionName { get; set; }
    }
}
