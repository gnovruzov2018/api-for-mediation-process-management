﻿using MediationProcessManagementAPI.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Dtos
{
    public class RequestCreateDto
    {
        public int ProfessionId { get; set; }
        public int[] DistrictIds { get; set; }
        public int[] MediatrIds { get; set; }
        public int? CourtId { get; set; }
        public SideCreateDto[] Sides { get; set; }
        public string ConflictInfo { get; set; }
        public string CourtCaseInfo { get; set; }
        public string PrefferedSessionTime { get; set; }
        public string RequiredLangs { get; set; }
        public bool CaseInAction { get; set; }
        public int? OfficeId { get; set; }
    }

    public class SideCreateDto
    {
        public string SideFirstName { get; set; }
        public string SideLastName { get; set; }
        public Gender SideGender { get; set; }
        public string AdvocateFirstName { get; set; }
        public string AdvocateLastName { get; set; }
        public string AdvocateMiddleName { get; set; }
        public string OrganizationName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public string SideMiddleName { get; set; }
    }
}
