﻿using MediationProcessManagementAPI.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models.Dtos
{
    public class RequestReturnDto
    {
        public string Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CourtName { get; set; }
        public Status Status { get; set; }
        public DateTime? AcceptDate { get; set; }
        public PersonDto Person { get; set; }
        public MediatrItemDto SelectedMediatr { get; set; }
        public OfficeReturnDto Office { get; set; }
        public List<MediatrItemDto> Mediatrs { get; set; }
    }

    public class RequestFullReturnDto
    {
        public string Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ConflictInfo { get; set; }
        public string CourtCaseInfo { get; set; }
        public string PrefferedSessionTime { get; set; }
        public string RequiredLangs { get; set; }
        public bool CaseInAction { get; set; }
        public bool MediatrSelected { get; set; }
        public DateTime? AcceptDate { get; set; }
        public OfficeReturnDto Office { get; set; }
        public Court Court { get; set; }
        public PersonDto Person { get; set; }
        public MediatrItemDto Mediatr { get; set; }
        public Status Status { get; set; }
        public ProfessionDto Profession { get; set; }

        public List<SideReturnDto> Sides { get; set; } //map
        public List<MediatrItemDto> Mediatrs { get; set; }
        public List<DistrictReturnDto> Districts { get; set; } //map
    }

    public class SideReturnDto
    {
        public int Id { get; set; }
        public string SideFirstName { get; set; }
        public string SideLastName { get; set; }
        public string SideMiddleName { get; set; }
        public Gender SideGender { get; set; }
        public string AdvocateFirstName { get; set; }
        public string AdvocateLastName { get; set; }
        public string AdvocateMiddleName { get; set; }
        public string OrganizationName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class RequestRejectDto
    {
        public string RejectText { get; set; }
    }
}
