﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Certificate : EntityBase
    {
        public string IssuerInstitution { get; set; }
        public string CertificateName { get; set; }
        public DateTime CertificationDate { get; set; }
        public string FilePath { get; set; }
        public int MediatrId { get; set; }

        public Mediatr Mediatr { get; set; }
    }
}
