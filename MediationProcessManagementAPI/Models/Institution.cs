﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Models
{
    public class Institution : EntityBase
    {
        public string InstitutionName { get; set; }
    }
}
