﻿using AutoMapper;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Mappings
{
    public class DistrictProfile : Profile
    {
        public DistrictProfile()
        {
            CreateMap<District, DistrictReturnDto>().ReverseMap();

            CreateMap<RequestedDistrict, DistrictReturnDto>()
                .ForMember(dest => dest.Id, act => act.MapFrom(src => src.District.Id))
                .ForMember(dest => dest.DistrictName, act => act.MapFrom(src => src.District.DistrictName));


        }
    }
}
