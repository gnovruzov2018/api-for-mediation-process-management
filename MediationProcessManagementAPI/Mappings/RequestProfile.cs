﻿using AutoMapper;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Mappings
{
    public class RequestProfile : Profile
    {
        public RequestProfile()
        {
            CreateMap<Request, RequestReturnDto>()
                .ForMember(dest => dest.SelectedMediatr, act => act.MapFrom(src => src.Mediatr))
                .ForMember(dest => dest.Person, act => act.MapFrom(src => src.User.Person))
                .ForMember(dest => dest.CourtName, act => act.MapFrom(src => src.Court.CourtName))
                .ForMember(dest => dest.Mediatrs, act => act.MapFrom(src => src.RequestedMediatrs));

            CreateMap<Request, RequestFullReturnDto>()
                .ForMember(dest => dest.Person, act => act.MapFrom(src => src.User.Person))
                .ForMember(dest => dest.Sides, act => act.MapFrom(src => src.Sides))
                .ForMember(dest => dest.Districts, act => act.MapFrom(src => src.RequestedDistricts))
                .ForMember(dest => dest.Mediatrs, act => act.MapFrom(src => src.RequestedMediatrs));
        }
    }
}
