﻿using AutoMapper;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Mappings
{
    public class MediatrProfile : Profile
    {
        public MediatrProfile()
        {
            CreateMap<Mediatr, MediatrItemDto>();

            CreateMap<RequestedMediatr, MediatrItemDto>()
                .ForMember(dest => dest.MediatrId, act => act.MapFrom(src => src.Mediatr.Id))
                .ForMember(dest => dest.FirstName, act => act.MapFrom(src => src.Mediatr.FirstName))
                .ForMember(dest => dest.LastName, act => act.MapFrom(src => src.Mediatr.LastName))
                .ForMember(dest => dest.MiddleName, act => act.MapFrom(src => src.Mediatr.MiddleName))
                .ForMember(dest => dest.ImagePath, act => act.MapFrom(src => src.Mediatr.ImagePath))
                .ForMember(dest => dest.RegistryNumber, act => act.MapFrom(src => src.Mediatr.RegistryNumber));

            CreateMap<MediatrCreateDto, Mediatr>();

            CreateMap<int, MediatrProfession>()
                .ForMember(dest => dest.ProfessionId, act => act.MapFrom(src => src));

            CreateMap<int, DistrictMediatr>()
                .ForMember(dest => dest.DistrictId, act => act.MapFrom(src => src));

            CreateMap<MediatrSocialMediaCreateDto, MediatrSocialMedia>();

            CreateMap<MediatrSocialMedia, MediatrSocialMediaDto>()
                .ForMember(dest => dest.SocialMediaName, act => act.MapFrom(src => src.SocialMedia.SocialMediaName));

            CreateMap<EducationCreateDto, Education>();
            CreateMap<Education, EducationDto>();

            CreateMap<CertificateCreateDto, Certificate>();
            CreateMap<Certificate, CertificateDto>();

            CreateMap<Mediatr, MediatrTableDataDto>()
                .ForMember(dest => dest.Districts, m => 
                    m.MapFrom(src => string.Join(", ", src.DistrictMediatrs.Select(x => x.District.DistrictName))));

            CreateMap<Mediatr, MediatrExtendedItemDto>()
                .ForMember(dest => dest.OfficeName, act => act.MapFrom(src => src.Office.OfficeName))
                .ForMember(dest => dest.InstitutionName, act => act.MapFrom(src => src.Institution.InstitutionName))
                .ForMember(dest => dest.Districts, act => act.MapFrom(src => src.DistrictMediatrs.Select(x => x.District.DistrictName)));
        }
    }
}
