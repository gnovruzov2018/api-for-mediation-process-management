﻿using AutoMapper;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Mappings
{
    public class ProfessionProfile : Profile
    {
        public ProfessionProfile()
        {
            CreateMap<Profession, ProfessionDto>();
            CreateMap<MediatrProfession, ProfessionDto>()
                .ForMember(dest => dest.Id, act => act.MapFrom(src => src.ProfessionId))
                .ForMember(dest => dest.ProfessionName, act => act.MapFrom(src => src.Profession.ProfessionName));
        }
    }
}
