﻿using AutoMapper;
using MediationProcessManagementAPI.Models;
using MediationProcessManagementAPI.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediationProcessManagementAPI.Mappings
{
    public class SideProfile : Profile
    {
        public SideProfile()
        {
            CreateMap<Side, SideCreateDto>().ReverseMap();
            CreateMap<Side, SideReturnDto>().ReverseMap();
        }
    }
}
